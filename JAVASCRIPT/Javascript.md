# ---- [ JAVASCRIPT ] ----

	[../index.md]

# RESOURCES
    https://www.javascripture.com/
    https://developer.mozilla.org/en-US/docs/Web/API/FileReader/readyState

## READ LOCAL FILE
    /* LOCAL METHOD */
    <input type="file" id="picker" onchange="load_local_file(this)" style="position:absolute;top:440px;left:0px;"></input>
    function load_local_file(input) {
      var file = input.files[0];
      var filereader = new FileReader();
      filereader.readAsText(file);
      filereader.onload = function() {
        document.getElementById("colorscheme").innerHTML = filereader.result;
      };
    }


## GET BY CLASSNAME
    var x = document.getElementsByClassName("game-wrapper");

## ARRAY
    var arr = {}
    arr[0] = "Hello";
    arr[1] = "Jack";

## OUTPUT TO CONSOLE
	console.log("Result  ", result);
 
 * Output multiple value in one line
 	console.log("X value: Y value: Result: ",x,y,result)
   or:
	console.log("Res expected: ", result , "Length " ,result.length)

## GET VALUE FROM INNERHTML
	console.log("Result Length ",res_input.innerHTML.length);

## GET NUMBER LENGTH
	var result = 123;
	console.log("Res expected: ",result,"Length ",result.toString().length)
	→ Print 3

## SPAWN NUMBER (BETWEEN MIN AND MAX)
  Math.random() * (max - min) + min;

## GET ALL INFOS ABOUT OBJECT WITH ID:
	var tmp = document.getElementById('calc');
	console.log("OBJ:  ",tmp);

 OUTPUT(webbrowser):
    innerHTML: "8-7 = "
    innerText: "8-7 =" 
    outerHTML: "<p id=\"calc\" class=\"calc\">8-7 = </p>"
    outerText: "8-7 ="
    ...
  * MODIFY IT:
   var tmp = document.getElementById('calc').outerHTML;

## SET PAUSE 
 ES6:  setTimeout(( ) => { alert("You' re wrong"); },"800");
 ES5: setTimeout(alert, 3000, 'setTimeout Demo!');
 
## RETURN MULTIPLE VALUE FROM FUNC
function spawn_calc() {
  var x = Math.floor(Math.random() * 100);
  var y = Math.floor(Math.random() * 100);
  result = x + y;
  return [x,y,result];
}

* Read returned Values

 var val = spawn_calc();
 var calc = document.getElementById('calc');
 calc.innerHTML= val[0] + " + " + val[1] + " = ";

