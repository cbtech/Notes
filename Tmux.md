# [ TMUX ] 

## Commands 
  -	`tmux list-commands`			            List All Commands 
  -	`tmux neww -c "#{pane_current_path}`	    Create new Window with current path
  -	`tmux kill-pane`				            Close Current Pane
  -	`tmux last-window / previous-window`	    Go to Last / Previous window		
  -	`tmux resize-pane -Z`			            Fullscreen Pane 
  -	`tmux setenv WAYLAND_DISPLAY wayland-0`	    Set env for TMUX
  - `tmux show-env`				                Show environment vars

**Simple popup**
	display-popup 

**Simple popup with filemanager**
	display-popup -E 'nnn'

## Swap pane instead window
 * Swap Pane  
    `swapp -t1` → Switch curent pane(right side) to the left  
    `swapp -s1 -t3`

## Move current pane  
   - `join-pane -t:2`           moves the current pane to window 2 of the current session
   - `join-pane -t CODING:2`    moves the current pane to window 2 of a session called CODING
   - `join-pane -s CODING:2.1`  moves the second pane of window 2 of session net-pers below the current panel
   - `join-pane -t 3.2`         moves the current pane after the third pane of window 3 of the current session.

## keybinding
   - `tmux splitw -h`		        Split Pane Horizontal
   - `tmux splitw -v`		        Split Pane Vertically
   - `tmux select-pane -LRDU`	    Split Pane Left/Right/D/U

* Merge all panes in one window  
    ```
    tmux join-pane -t Mysession:0 -s 1  
    tmux join-pane -t Mysession:0 -s 2  
    tmux join-pane -t Mysession:0 -s 3  
    ```
## Sessions
  - `tmux ls`           List sessions     
  - `tmux a -t 0`       Attach session 0  
  - `tmux detach`       Detach session  


