# Chess Openings for White

## 1. e4 Openings

### Ruy Lopez (Spanish Game)
1. e4 e5
2. Nf3 Nc6
3. Bb5 a6
4. Ba4 Nf6
5. O-O Be7
6. Re1 b5
7. Bb3 d6
8. c3 O-O
9. h3 Nb8
10. d4

### Italian Game
1. e4 e5
2. Nf3 Nc6
3. Bc4 Bc5
4. c3 Nf6
5. d4 exd4
6. cxd4 Bb4+
7. Nc3 Nxe4
8. O-O Bxc3
9. d5 Ne7
10. bxc3

### Sicilian Defense
1. e4 c5
2. Nf3 d6
3. d4 cxd4
4. Nxd4 Nf6
5. Nc3 a6
6. Be2 e5
7. Nb3 Be7
8. O-O O-O
9. f4 exf4
10. Bxf4

## 2. d4 Openings

### Queen's Gambit Declined
1. d4 d5
2. c4 e6
3. Nc3 Nf6
4. Bg5 Be7
5. e3 O-O
6. Nf3 h6
7. Bh4 b6
8. cxd5 exd5
9. Bd3 c5
10. O-O

### King's Indian Defense
1. d4 Nf6
2. c4 g6
3. Nc3 Bg7
4. e4 d6
5. Nf3 O-O
6. Be2 e5
7. O-O Nc6
8. d5 Ne7
9. Ne1 Nd7
10. Be3

## 3. c4 Openings

### English Opening
1. c4 e5
2. Nc3 Nf6
3. g3 d5
4. cxd5 Nxd5
5. Bg2 Nb6
6. Nf3 Nc6
7. O-O Be7
8. d3 O-O
9. Be3 Be6
10. Rc1

## 4. Nf3 Openings

### Reti Opening
1. Nf3 d5
2. c4 e6
3. g3 Nf6
4. Bg2 Be7
5. O-O O-O
6. d4 c6
7. Qc2 Nbd7
8. Rd1 b6
9. b3 Bb7
10. Bb2

## 5. f4 Opening

### Bird's Opening
1. f4 d5
2. Nf3 Nf6
3. e3 g6
4. b3 Bg7
5. Bb2 O-O
6. Be2 c5
7. O-O Nc6
8. Qe1 d4
9. Na3 Nd5
10. Nc4

## 6. b3 Opening

### Nimzowitsch-Larsen Attack
1. b3 e5
2. Bb2 Nc6
3. e3 d5
4. Bb5 Bd6
5. f4 Qe7
6. Nf3 exf4
7. Bxg7 fxe3
8. O-O exd2
9. Nbxd2 Be6
10. Re1 O-O-O

## 7. g3 Opening

### King's Fianchetto Opening
1. g3 d5
2. Bg2 Nf6
3. Nf3 c6
4. O-O Bf5
5. d3 e6
6. Nbd2 h6
7. Qe1 Be7
8. e4 Bh7
9. Qe2 O-O
10. b3

## 8. Uncommon and Rare Openings

### Sokolsky (Orangutan) Opening
1. b4 e5
2. Bb2 f6
3. e3 Bxb4
4. Qg4 Bf8
5. Qh5+ g6
6. Qh4 Nc6
7. f4 d6
8. Nf3 Be6
9. Bb5 Qd7
10. O-O

## References
- [List of chess openings](https://en.wikipedia.org/wiki/List_of_chess_openings)

