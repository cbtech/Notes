# [ANDROID UI COMPONENTS ]

**wrap_content**  
    when wrap_content is defined with layout_width that means that the layout will have 
    the size of the content

**match_parent(fill_parent)**  
    set the view according to the parent size.

**dp(Density-Independent Pixels)**  
    an abstract unit that is based on the physical density of the screen
    These units are relative to a 160dpi screen, so 1 dp is 1 pixel on a 160 dpi screen.

**sp(Scaleable Pixels)**  
    This is like the dp unit, but it also scaled by the user's font size preference.
    It is recommended you use this unit when specifying font sizes.

## UI Elements 
**LayoutInflater**:  
    Create view from XML layout. Instantiates a layout XML file into a view objects.  
    It never used directly. Instead use `Activity.getLayoutInflater()` or `Context.getSystemService`
    to retrieve a standard layoutInflater instance that is already hooked up to the current context
    and correctly configured for the device you are running on.

**RecyclerView** is equivalent to **ListView**.  
   Recyclerview display large sets of data.You supply the data and 
   define how each item looks. RecyclerView is the ViewGroup that contains 
   the view corresponding to your data.Each individual element in the list 
   is defined by a view holder object. When the view holder is created, it 
   doesn't have any data associated with it. After the view holder is created,
   the RecyclerView binds it to its data.

 * Step to implementing your RecyclerView.
   1.  Decide how the list or grid looks.You can use one of the RecyclerView
       library standard layout.
   2.  Design how each element in the list looks and behaves.Your version of 
       viewholder provides all the functionnality for your list items.Your 
       ViewHolder is a wrapper around a view, and that view is managed by 
       RecyclerView.
   3.  Define the Adapter that associates your data with the ViewHolder view.

 * Plan Your layout.
    The items in your RecyclerView are arranged by a LayoutManager class.
    See **LAYOUT section below** 
   
  - **Implement your adapter and viewHolder.**  
      Once your determine your layout, you need to implement your Adapter
      and your View Holder.These two classes work together to define 
      how your data is displayed. The Viewholder is wrapper arround a View 
      that contains the layout for an individual item in the list. The adapter 
      creates ViewHolder objects as needed and also sets the data for those view
      The process of associating views to their data is called binding
    
  - **When you define your adapter, you override three key methodes:**  
    `OnCreateViewHolder` → RecyclerView calls this method whenever it
                         needs to create a ViewHolder.  
    `OnBindViewHolder` → RecyclerView calls this method to associate a 
                       viewHolder with data.  
    `GetItemCount()` → RecyclerView calls this method to get the size of 
                     the dataset.Example in address book app, this might be the total
                     number of adresses.  
   
**Edittext** → PlainText

## VIEWS
**VIEW Class**  
  A view occupies a rectangular area on the screen 
  and is responsible for drawing and event handling
  View is a base class for widget
    
* Difference between Activity and View.
  View is display system of android where you define layout to 
  put subclasses of View in it (Buttons,Images...). Activity is 
  Screen system of Android where you put display.
    
## LAYOUT
  LinearLayoutManager → arranges the items in one-dimensional list
  GridLayoutManager → arranges the items in a two-dimensional grid
  StaggeredGridLayoutManager → Same as GridLayoutManager.


