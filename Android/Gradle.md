# [ Gradle Build Tool ]

## Resources
    https://docs.gradle.org/current/userguide/getting_started_eng.html

## Commands
'''
    gradle -v show commands
'''

## Gradlew 
 The wrapper is a script that invoke a declared version of gradle.
```  
  ./gradlew run                                 → Run  
  ./gradlew clean                               → Clean Gradle cache  
  ./gradlew tasks                               → List all available tasks
  ./gradlew :app:dependencies                   → View all dependencies for the app
  ./gradlew dependencies                        → Show Dependencies  
  ./gradlew --version                           → Show gradle version  
  ./gradlew build                               → Build project
  ./gradlew wrapper --gradle-version 7.2        → Update Gradle version  
  ./gradlew assembledebug                       → Building Application(like play button in Astudio)

```

## SHOW KOTLIN VERSION
 Add this to **build.gradle.kts**
 ```
   tasks.register("kotlinVersion") {
    doLast {
     println("Kotlin version: ${KotlinVersion.CURRENT}")
    }
   }

   ./gradlew kotlinVersion 
```

## Settings file(settings.gradle.kts → Kotlin / settings.gradle → grooby DSL)
  The primary purpose of the settings file is to add subprojects to your build.
  Gradle supports single and multiproject builds.
   
   * For single-project build, the settings file is optional.
   * For multi-project build, the settings file is madatory and declares all subproject.
 
 **Example of settings.gradle.kts**
```
    rootProject.name = "root-project"
    include("sub-project-a")
    include("subproject-b")
    include("subpoject-c")
```

## Build Script(build.gradle.kts → Kotlin / build.gradle → GroobyDSL)
   **Example of build.gradle.kts**
   ```
    plugins {
      id("application")
    }
    application {
     mainClass = "com.example.Main"
    }
   ```
## Dependency Management Basics
 Dependency management is an automated technique for declaring and resolving external ressources
 required by a project.**Gradle Build script** define the process to build projet that may require
 external dependencies.**Dependencies** refer to JAR, plugins, libraries.
 
 ### Version catalog
  Version catalogs provide a way to centralize your dependency declarations in a `libs.versions.toml` file.  
  The file located at <project> **/gradle/libs.version.toml**.

  **Example of libtoml**
```
     [versions]
      androidGradlePlugin = "7.4.1"
      mockito = "2.16.0"

     [libraries]
      googleMaterial = { group = "com.google.android.material", name = "material", version = "1.1.0-alpha05" }
      mockitoCore = { module = "org.mockito:mockito-core", version.ref = "mockito" }

      [plugins]
      androidApplication = { id = "com.android.application", version.ref = "androidGradlePlugin" }
```
 1. **[version]** declare the version numbers that plugins and libraries will reference.
 2. **[libraries]** define the libraries used in the build files.
 3. **[plugins]** define plugins

 ### Declaring Your Dependencies
  To add a dependency to your project, specify  a dependency in the depencies block of your `build.gradle.kts`
  The folowing `build.gradle.kts` add a plugin and two dependencies to the project using the version catalog above.

```
plugins {
   alias(libs.plugins.androidApplication)  
}

dependencies {
    // Dependency on a remote binary to compile and run the code
    implementation(libs.googleMaterial)    

    // Dependency on a remote binary to compile and run the test code
    testImplementation(libs.mockitoCore)   
}
```
   
## Tasks
 A task represents some independent unit of work that a build performs,such as compiling classes, creating a JAR,
 generating Javadoc or publishing archives to a repository.

## Plugin Basics  
 **See:** https://docs.gradle.org/current/userguide/plugin_reference.html#plugin_reference
 
 Gradle is built on  a plugin system. A **plugin** is a **piece of software** that provide additionnal 
 functionnality to the **Gradle build System**.


