# [ SDK platform Tools ]

## SDKManager
    If you doesn't want to use an IDE Like androidstudio. You need to have
    sdkmanager to update, view,install .

## Docs
See:    https://developer.android.com/studio/run/emulator-commandline  
See:    https://developer.android.com/tools/avdmanager  
See:    https://developer.android.com/tools/sdkmanager  
https://medium.com/michael-wallace/how-to-install-android-sdk-and-setup-avd-emulator-without-android-studio-aeb55c014264
https://gist.githubusercontent.com/mrk-han/66ac1a724456cadf1c93f4218c6060ae/raw/5559c18ee37df76059045cfcdf061cdec20612fc/emulator-install-using-avdmanager.md

## Install Tools
 Download platform-tools to have adb...(or get it via sdkmanager)  
    https://developer.android.com/tools/releases/platform-tools#downloads  

 Goto to the end of page and download commandline tools(avdmanager/sdkmanager...)  
    https://developer.android.com/studio  

Tree of Android SDK → --sdk_root=/home/cbtech/Android/SDK
```
    Android
     Sdk
     build-tools                                → Necessary tools build your Android App
           <version>33.0 | 34.0| 35.0 ...
     cmdline-tools                              →  Commandline Tools
     apkmanager/avdmanager/sdkmanager
     emulator
     emulator/qemu-img ...
     licenses
     platforms              → This folder is required to compile your app for specified API Level.
     platforms-tools        → Some tools to communicate with Android devices when you plug themin your computer.  
     skins
     source
     system-images          → Android Images used in Emulator
```
## Commands of SDKManager
 * Install platform and build tools for android-33  
  `sdkmanager "platform-tools" "platforms;android-33" "build-tools;33.0.0"`
    
 * List SDK pkg installed
  `sdkmanager --list --sdk_root="/home/cbtech/Android/SDK/cmdline-tools/bin/"`  

 * Install emulator and image system
   `sdkmanager --install "system-images;android-33;google_atd;arm64-v8a" --sdk_root=/home/cbtech/Android/SDK`
 
 * List Images system 
    `sdkmanager --list --sdk_root=/home/cbtech/Android/SDK | grep system-images`  

    For Intel use x86_64 image, for Macbook M1/M2 use arm64-v8a
    It's recommended always using the new google_atd or aosp_atd images when possible. There are 40% more 
    efficient than the google_apis image.

## AVD (Android Virtual Device) Emulator
 * Create Emulator
   `avdmanager create avd --name <name of emulator> --package  "system-images;android-33;google_atd;x86_64"`

**List Devices**
``` 
    avdmanager list avd             List emulators with infos
    emulator -list-avds             Just show emulators name 
    emulator -avd <name>            Start emulator
```
```
    adb devices
```
You should see a device like <emulator-5554>

**Install APK on VirtualDevice**
```
    adb install <path-to-APK>
```

## UDEV RULES
**lsusb**  
```
    Bus 005 Device 009: ID 2717:ff48 Xiaomi Inc. Mi/Redmi series (MTP + ADB)
```

* Create **/etc/udev/rules.d/51-android.rules**
```
    SUBSYSTEMS=="usb", ATTRS{idVendor}=="2717", ATTRS{idProduct}=="ff48", MODE="0666", 
                GROUP="androiddev", SYMLINK+="android%n"
    adb start-server → Start the server allow connection on the phone 
```

