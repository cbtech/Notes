# [ ANDROID DEV ] 

   [Layout components](UIDesign.md)

Go to https://developer.android.com/studio
    and download Commandline tools only

# PHONE VERSION 
   6.52in 720x1600 24 bits

## CONFIGURATION (FREEZE AND BUG)
  **File > PowerSave Mode →** Disable indexing to avoir bug
  **File > Help >** Edit Custom Properties 
```
  -Xms256m
  -Xmx2048m
  -XX:ReservedCodeCacheSize=240m
```
* Enable memory Indicator
   **File View > Appearence >StatusBar widget > Memory Indicator**



## MAVEN REPOSITORY
        maven { url = uri("https://maven.scijava.org/content/repositories/public/") }

### What is non Ivy External components ???
   **Ivy External components** is repo that follow **ivy format** to *store artifact and meta-data*.  
   Maven repo is non Ivy External Components.


## DEPENDENCIES 
 **Plugins** section influences your build process.  
 **Dependencies** influence your code.  

https://developer.android.com/build/dependencies#dependency_configurations

The code to include is in this page. At the bottom of page runtimeOnly ...  
https://mvnrepository.com/artifact/com.google.android.material/material/1.12.0

**app.compat:**  
    https://developer.android.com/jetpack/androidx/releases/appcompat

**androidx.contraintLayout:**
    Powerful and flexible tool for android app UI. it helps to create complex layout 
    using constraints to position UI elements relative to one another.

**Material:**
    https://m2.material.io/develop/android/docs/getting-started
    Material Design Components provides UI components that follow the Material

Design guidelines. This includes:
        Buttons, Switches, and Sliders
        Text fields and input components
        Navigation elements like Navigation View, Bottom Navigation, and Tabs
        Dialogs and Snackbars
        Floating Action Buttons (FAB)
        Themed styles and theming helpers
        Progress indicators, and more

**ProGuard** (pro-rules in build.gradle.kts)  
   Proguard works for:  
   - Shrinking    -  Removing unused code and ressources.  
   - Optimizing   -  Making the code run faster and more efficiently.  
   - Obfuscating  -  Renaming Classe,fields, and methods to make the coder harder.  
                        to understand if decompiled(Make the app harder to reverse-engineer).  
   - Pre-verifying - Preparing the code for java-runtime environments taht require verified code.  
    Maven is  repository of libraries.  

* Build app → Build Menu → MakeProjects(Ctrl + F9) click on hammer(makeModule) icon. 

* Activity is a components that represent interface(UI)
    main_activity is located under /res/layout/

* Service is a daemon that runs in background .
       - Example of service:
            Audio player application
            Access location information
            Client-server side interface between applications or IPC.

* Broadcast receiver:
        System events that you can listen to using  broadcast receivers:
            Device boot
            Device screen on and screen off
            Date and Locale specific change 
            Push notification
            Communicate with other application.

* Content provider are components that provide you a way to access the 
        sta stored byt itself or stored by other applications.

* Content resolver is used to resolve request for data access from other app 
    Fragments and view can help us to create UI. View is lighter than fagment and the 
    view lifecycle is notwith the activity default
    ANR(Application Not Responding error) 

    res folder is for drawable and layout(screen design )...

* Gradle file
    The first gradle file is for project specific configurations.In general, configurations
    added to this file will be applied to each module used in the application.

    The second gradle file contains configurations/ dependencies.
    
## Logcat
   Logcat is a tool that show log one th phone  
    
**To view the log** 
```
    adb logcat
```    
**To clear the device log**
```
    adb logcat -c 
    adb logcat --clear
```

**Print only log DEBUG level**
```
    adb logcat *:D
```

## LINT used for:
  - Deprecated code used in your code
  - Unused code 
  - Unused imports
  - Incorect use of access specifiers for memebers of the class
  - Code snippets that can cause the application to crash on different Android
  - Unhandled excepetions.
  
 **Start from Android studio** <menubar> Code → Inspect code
 **in Commandline** ./gradlew lint

* Activate device mirroring 
    Go to Settings (CTRL+Alt+S)
    Go to Tools section → Device mirroring → Activate mirroring when a new physical device is connected.

* Install SDK
    Settings Langages → and Frameworks → Android SDK 
    Click edit to select SDK folder.

**page 86**
    
    
## TROOBLESHOOTING 
  **Unresolved reference Intent** → import android.content.Intent

  * Phone disconnecting after gradle build.
     Go to: Android Studio Jellyfish Settings -> Build, Execution, Deployment 
     -> Debugger and change ADB Server Usb Backend from Default to Native.
