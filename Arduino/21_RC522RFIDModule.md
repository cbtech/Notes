# 21 RC522 RFID Module

## RFID library

https://www.arduinointro.com/articles/projects/how-to-use-rfid-rc522-with-arduino-a-complete-beginners-guide

  **RFID(Radio Frequency Identification)** is a technology that used electromagnetic fields  
      to automatically identify and track tags attached to objects.The RC522 is a highly  
      integrated reader/writer IC for contactless communication at 13.56Mhz.  

* Principal Application of RFID:
- Acces Control System(Secure home or office)
- Attendance systems(Track employee or student attendanceby scanning RFID cards.
- Inventory Management(Automate your inventory tracking process by scannning 
                RFID taggeditems.

- `arduino-cli lib install "MFRC522"`

 Manually, download rfid lib in https://github.com/miguelbalboa/rfid.  
 Put arduino-cli in `~/Arduino/rfid/rfid-1.4.11/examples` and build using this command 
  `./arduino-cli_1.0.4 compile -b arduino:avr:mega DumpInfo/ --build-properties 
                                  compiler.cpp.extra_flags="-I." --libraries ../../src/ 
                                  -v --output-dir .`


