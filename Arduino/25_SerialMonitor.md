# -------------- [ SERIAL MONITOR ] ---------------

    [../../WORK/Arduino/Serialcom/Serialcom.ino] (Serial monitor using picocom)

# COMMUNICATION WITH PICOCOM
    1. Initialising the usart protocol(code from scratch)
            see [../WORK/Arduino/Led_Hard/led.c] 

    2. Communicate with PICOCOM
        picocom -r -b 115200 /dev/ttyACM0

    Stop Communication Ctrl-A Ctrl-X

