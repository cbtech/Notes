# ---- [ WINDOWS ] ----

## INSTALL WINDOWS 10 FROM LINUX
    Download woeUSB
     https://github.com/WoeUSB/WoeUSB/releases

    Install wimlib / parted
   
    Run command:
        ./woeusb --device /home/cbtech/Downloads/Win10_22H2_French_x64v1.iso /dev/sdf --tgt-fs NTFS

## ACTIVATE WINDOWS 10
    https://github.com/massgravel/Microsoft-Activation-Scripts
    https://massgrave.dev/
