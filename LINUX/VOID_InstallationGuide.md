# ---- [ Void Linux installation guide ] ----

## Live image

### Logging in
    User:anon Pass:voidlinux
    User:root Pass:voidlinux

### Setting keyboard layout
```sh
loadkeys fr 
```
### 
```sh
vi /etc/passwd
change sh to bash for root
```

### Connecting to the internet
```sh
wpa_passphrase <ssid> <passphrase> | tee -a /etc/wpa_supplicant/wpa_supplicant-<interface>.conf
sv restart dhcpcd
ip link set up <interface>
```

### Formatting disk
Run:
```sh
fdisk /dev/sda
```
Then:
1. Select `g` to generate a GTP table
2. Select `n` to create the EFI partition with size of +200M
3. Change the created partition's type by selecting `t` and then selecting the option that represents `EFI Partition`
4. Select `n` to create the GRUB partition with size of +500M
5. Select `n` to create the `btrfs` partition with the remaining size

### Creating the filesystems
```sh
mkfs.vfat -n boot -F 32 /dev/sda1
mkfs.ext2 -L GRUB /dev/sda2
```

### Mounting the `btrfs`partition and creating subvolumes 
```sh
mount /dev/sda3 /mnt
mkdir -p /mnt/home
mkdir /mnt/boot
mkdir /mnt/boot/efi
mount -o rw,noatime /dev/sda1 /mnt/boot/efi
```

### Installing the base system
#### Using `glibc`
```sh
xbps-install -Sy -R https://repo-de.voidlinux.org/current -r /mnt base-system btrfs-progs cryptsetup grub-x86_64-efi
```
#### Using `musl`
```sh
export XBPS_ARCH=x86_64-musl
xbps-install -Sy -R https://alpha.de.repo.voidlinux.org/current/musl -r /mnt base-system btrfs-progs cryptsetup grub-x86_64-efi
```

#### Running `chroot`
```sh
mount -t proc proc /mnt/proc
mount -t sysfs sys /mnt/sys
mount -o bind /dev /mnt/dev
mount -t devpts pts /mnt/dev/pts
cp -L /etc/resolv.conf /mnt/etc/
cd /mnt
chroot /mnt /bin/bash
passwd root
chown root:root /
chmod 755 /
echo <hostname> > /etc/hostname
```

Then edit the file `/etc/rc.conf` to contain the following:
```ini
HOSTNAME="<hostname>"

# Set RTC to UTC or localtime.
HARDWARECLOCK="UTC"

# Set timezone, availables timezones at /usr/share/zoneinfo.
TIMEZONE="America/Sao_Paulo"

# Keymap to load, see loadkeys(8).
KEYMAP="br-abnt2"

# Console font to load, see setfont(8).
#FONT="lat9w-16"

# Console map to load, see setfont(8).
#FONT_MAP=

# Font unimap to load, see setfont(8).
#FONT_UNIMAP=

# Kernel modules to load, delimited by blanks.
#MODULES=""
```

Then run the following commands:
```sh
echo 'en_US.UTF-8 UTF-8' > /etc/default/libc-locales
export GRUB_UUID=$(blkid -s UUID -o value /dev/sda2)
export UEFI_UUID=$(blkid -s UUID -o value /dev/sda1)
export ROOT_UUID=$(blkid -s UUID -o value /dev/mapper/cryptoroot)
export LUKS_UUID=$(blkid -s UUID -o value /dev/sda3)
cat <<EOF > /etc/fstab
UUID=$GRUB_UUID /boot ext2 defaults,noatime 0 2
UUID=$UEFI_UUID /boot/efi vfat defaults,noatime 0 2
UUID=$ROOT_UUID / btrfs rw,noatime,ssd,compress=zstd,space_cache,commit=120,subvolid=257 0 1
UUID=$ROOT_UUID /home btrfs rw,noatime,ssd,compress=zstd,space_cache,commit=120,subvolid=258 0 2
UUID=$ROOT_UUID /.snapshots btrfs rw,noatime,ssd,compress=zstd,space_cache,commit=120,subvolid=259 0 2
EOF
echo "GRUB_CMDLINE_LINUX=\"${LUKS_UUID} i915.modeset=1\"" >> /etc/default/grub
echo 'add_dracutmodules+="crypt btrfs resume"' >> /etc/dracut.conf
echo 'tmpdir=/tmp' >> /etc/dracut.conf
dracut --force --hostonly --kver <kernel-version>
grub-mkconfig -o /boot/grub/grub.cfg
grub-install --target=x86_64-efi --efi-directory=/boot/efi --bootloader-id=void_grub --boot-directory=/boot --removable --recheck --debug /dev/sda
xbps-install -S void-repo-nonfree
xbps-install -Su
xbps-install intel-ucode
echo 'early_microcode="yes"' >> /etc/dracut.conf.d/intel_ucode.conf
xbps-reconfigure -f linux<kernel-version-major.minor>
exit
exit
umount -R /mnt
reboot
```

Note: one can also add `splash quiet` to `GRUB_CMDLINE_LINUX_DEFAULT` in `/etc/default/grub`.

## Post-installation
### Creating the main user and adding them to sudoers
Log in as root
```sh
xbps-install -S zsh
usermod -m -G wheel -s $(which zsh) <username>
passwd <username>
visudo
```
After running `visudo`, uncomment the line that contains `%wheel`. Log out and log in with the newly created user.

#### Locking the root account
:warning: **Important** :warning:  
Only run this after setting up the main user!
```sh
sudo passwd -dl root
```

### Connecting to the internet
Run the proper `wpa_supplicant` steps to set Wi-Fi or stuff you need. Then, register and start the needed services:
```sh
sudo ln -s /etc/sv/dhcpcd /var/service/
sudo ln -s /etc/sv/wpa_supplicant /var/service/
```
By symlinking the services, runit will pick them up in the next five seconds and start them!

### Installing drivers needed by Xorg
```sh
sudo xbps-install -S xorg-minimal xorg-fonts \
    xf86-video-intel xf86-video-ati \
    xf86-input-evdev xf86-video-fbdev \
    xf86-input-synaptics
echo 'exec bspwm' >> ~/.xinitrc
```

#### Rootless Xorg
First, check whether [KMS](https://en.wikipedia.org/wiki/Mode_setting#Linux) is enabled:
```sh
sudo cat /sys/module/i915/parameters/modeset
```
If it returns `1`, then KMS is enabled. If not, add `i915.modeset=1` to the kernel parameters.
Then, ensure `/etc/X11/Xwrapper.config` has `needs_root_rights = no` and add the user to the following groups:
```sh
sudo usermod -aG input,video <username>
```

Finally, to start Xorg, you should run, without sudo:
```sh
startx -- vt1
```
https://wiki.voidlinux.org/Xorg
