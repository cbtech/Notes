# ------------------ [ OPENSUSE ] -----------------

## MPV Issues(video freeze)
  * Add packman Repository
    sudo zypper ar -cfp 90 http://ftp.gwdg.de/pub/linux/misc/packman/suse/openSUSE_Tumbleweed/ packman
    sudo zypper refresh

    sudo zypper install --from packman gstreamer-plugins-bad gstreamer-plugins-ugly gstreamer-plugins-libav ffmpeg-7

