# ---- [ VOIDLINUX ] ----

    [VOID_InstallationGuide.md] (Installation using chroot)

# [ XBPS MANAGER ]
    xbps-install w3m     Installs and update pkg
    xbps-install -Su     Update system

 * XBPS-QUERY
   xbps-query             Searches for and display information about pkg
   xbps-query  -Rs        Search available repositories for pkg
   xbps-query -o "*/gcc"  List all gcc binary pkg
              -R          Search repositories 
               s          Search for locally-installed pkg
              -f          List files installed by pkg
              -l          List packages installed
              -o          List binary pkg

  * To get a list of all installed packages, without their version:
   `xbps-query -l | awk '{ print $2 }' | xargs -n1 xbps-uhelper getpkgname`

 * XBPS-REMOVE 
    xbps-remove           Remove all installed pkg
    xbps-remove -o        Remove orphan package

 * XBPS-RECONFIGURE

    xbps-reconfigure      Run the configuration steps for installed pkg
    xbps-reconfigure -fa  Reconfigure All pakages

  `xbps-alternatives`  Lists or sets the alternatives provided by installed pkg
  `xbps-pkgdb`         Can report and fix issues in the package db 
    
    xbps-pkgdb -m repolock linux    Block system update for this pkg
    xbps-pkgdb -m repounlock linux  Unlock Block update


* RECONFIGURE CERTIFCATES
    `xbps-reconfigure -f ca-certificates`

* PURGE OLD KERNEL(CLEAN)
    vkpurge list    → List kernel
    vkpurge rm all  → Remove all old kernels

## DISABLE TTY
    remove /var/service/aggetty-6 ...

## NETWORK CONFIGURATION
    * Edit /etc/dhcpcd.conf
    	interface enp2s3
	static ip_adress=192.168.2.10
	static routers=192.168.2.1
	static domain_name_server=172.20.10.1   (gateway)

## DOWNGRADING 
   * Add the pkg version to your local repository:
      `xbps-rindex -a /var/cache/xbps/pkg-1.0_1.xbps`

   Then downgrade with xbps-install
      `xbps-install -R /var/cache/xbps -f pkg-1.0_1`

   The -f flag is necessary to force downgrade

## HOLDING A PKG 
   To prevent a pkg from being update during a system update
      `xbps-pkgdb -m hold <pkg>`

  The hold can be remove with:
      `xbps-pkgdb -m unhold <pkg>`

## REPOSITORY LOCKING PKG  
  * If you've used xbps-src to build and install a pkg from a customized
  template. You may wish to prevent system update for this pkg
      `xbps-pkgdb -m repolock <pkg>`

* To remove the repolock
      `xbps-pkgdb -m repounlock <pkg>`

## IGNORING PKG 
  Sometimes you may wish to remove a pkg whose functionality is being
  provided by another pkg.Example: You may wish to use doas instead sudo
  but will be unable to remove the sudo pkg due to its dependencies.
  To remove it, you will need to ignore the sudo pkg

## VIRTUAL PKG 
  Virtual pkg can be created with xbps.d virtual pkg entry.
  For example , to create linux virtual package which will resolve 
  to the linux-5.6 pkg, create and xbps.d configuration file
  with the contents
             `virtualpkg=linux:linux5.6`

## LOGS
    xbps-install -S socklogd

    * Create service file /etc/sv/socklogd(-rwxr-xr-x)
     #!/bin/sh
     exec socklog unix /dev/log
    or 
     socklog-conf unix nobody cbtech  
     → Create directory (var/log/socklog) if doesn't exist

    * Run service
     ln -s /etc/sv/socklogd /var/service
     ln -s /etc/sv/socklogd /etc/runit/runsvdir/default
    

    all logs are stored in /var/log/socklog

    * TO SEE MSG ABOUT SYSTEM
        tail -f /var/log/messages

    * See kernel Msg
        cat /proc/kmsg

    List Kernel modules
        find /lib/modules/$(uname -r) -name '*.ko.zst' | xargs -P2 -I{} basename {} .ko.zst
## TROUBLESHOOTING
    * USING MAGIC SYSRQ KEY (Recover from freeze or cleanly restart your system)
        Sysrqkey is Printscreen key.

        cat /proc/sys/kernel/sysrq  → 1 means that sysrq is enabled
        echo "1" > /proc/sys/kernel/sysrq → to enable it
    
        Alt + SysRq + r Takes the keyboard out of raw mode. 
        Alt + SysRq + k Kill all programs on the current virtual console.
        Alt + SysRq + r e i s u b   Restarting your System

    ! [reposync] failed to fetch file 
       https://alpha.de.repo.voidlinux.org/current/aarch64/aarch64-repodata':
       Operation not permitted
    
    * TIME AND DATE not configured
      enable chronyd service → ln -s /etc/sv/service /var/service
  

  SSL Problems:
  	SSL_NO_VERIFY_PEER=true xbps-install -Syu
