# FFMPEG VIDEO EDITING

## Commandline Args 
```sh
   -f x11grab                      Record from screen
   -s 1280x1024                    Record Size
   -r 30                           Frame per second
   -f pulse/alsa                   Record audio from pulse or alsa
   -i default                      Uses delfault input device for recording
   -c:v libx264                    Setting Video Codec
   -c:a flac                       Setting Audio Codec
   -y                              Overwrite output file
```

## List Codecs
```sh
   ffmpeg -codecs | grep mp3  
   ffmpeg -encoders  
   ffmpeg -decoders  
   ffmpeg -formats  
   ffmpeg -h encoder=libx264           Show details about encoder
```
## Useful commands
**Extract pics from movies**  
   `ffmpeg -i input.mp4 -qscale:v 2 output_%03d.jpg`

**~ 60 secs**  
  `ffmpeg -ss 60 -i input.mp4 -qscale:v 4 -frames:v 1 output.jpg` 

### Create movie from only one picture
  `ffmpeg -loop 1 -i Disclaimer.png -c:v libx264 -t 30 -pix_fmt yuv420p -vf scale=1920:1080 Disclaimer.mp4`

   **t → time in second**  
 * Create a slideshow with img 01.png,02.png... with 1s(framerate 1)  
    `ffmpeg -framerate 1 -i %02d.png -vf "scale=1280:720" -c:v libx264 -pix_fmt yuv420p -r 30 output.mp4`
        
### View informations about file
   `ffprobe mediafile.mp3`

### mp3 to wav
   `ffmpeg -i song.mp3 song.wav`

### m4a to mp3
   `ffmpeg -i input.m4a -c:v copy -c:a libmp3lame -q:a 4 output.mp3`

### Transform all mp4/m4a in mp3 
```sh
     for f in *.mp4;
        do ffmpeg -i "$f" -q:a 0 -map a "Output/${f%.mp4}.mp3"
     done
```
```sh
    for f in *.mp4;
       do ffmpeg -i "$f"  -c:a libmp3lame "Output/${f%.m4a}.mp3"
    done
```
### merge two movies into one
```
    echo file 'file1' >> mylist 
    echo file 'file2' >> mylist 

    ffmpeg -f concat -i mylist.txt -codec copy output.mp4
```
### Speed up video
   **0.5(x2) 0.25(x4)**  
   `ffmpeg -i input.mkv -filter:v "setpts=0.5*PTS" output.mkv`

 **Speed DOWN**  
   `ffmpeg -i input.mkv -filter:v "setpts=2.0*PTS" output.mkv`

## Screencast 
  - Screencast under WAYLAND  
    `xbps-install mesa-vaapi`

  - Under wayland using kmsgrab
```
    ffmpeg -f alsa -ac 1 -i hw:CARD="mic" -f kmsgrab -i - -vf 'hwmap=derive_device=vaapi,scale_vaapi=w=1920:h=1080:format=nv12' \ 
           -c:v h264_vaapi -b:v 2500k -maxrate 4000k -y output-file.mkv
```
 - Under X11
```sh
                ffmpeg -y -f x11grab -s $(xdpyinfo | grep dimensions | awk '{print $2;}') \
                -i :0.0 \
                -f alsa -i default \
                -c:v libx264 -r 30 -c:a libmp3lame $filename 
```
**[Errors]**
   No handle set on framebuffer: maybe you need some additional capabilities?  
       `setcap cap_sys_admin+ep /usr/bin/ffmpeg`

   * Show audio source
     `ffmpeg -sources alsa`

## Record video
   `ffmpeg -y -i /dev/video0 output.mkv`

## Record audio 
```sh
    arecord -L              List recording audio sources
```
 **Capturing using FFMpeg**  
```sh
        ffmpeg -f alsa -ac 1 -i hw:CARD="mic" testmic.wav
    or
        ffmpeg -f alsa -i sysdefault:CARD=mic testmic.mp3
```
**Test using arecord**  
    `arecord -f S16_LE -r 48000 --device="sysdefault:CARD=mic" test_mic out.wav`  


