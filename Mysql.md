# ---- [ MYSQL ] ----

## LIST DATABASE
 SHOW DATABASES;
 
 :Output:
 ========
	Database:
		crypto
		information_schema
		mysql
		...

 * TO SELECT CRYPTO DB
      USE crypto

 * Show TABLES
  	SHOW TABLES;
	SHOW FULL TABLES

 :Output:
 ========
	Tables_in_crypto
		blockchain
		user
		users

 * SHOW INFORMATION IN TABLES
   	select * from users

 :Output:
 ========
	name		Email		username	 password
	John Doe	jd@gmail.com	johndoe		hash

 * DELETE ENTRY FROM TABLES
	DELETE FROM user WHERE name='John Doe';

  * DELETE TABLE(Remove 2 tables)
  	DROP TABLE users;
	DROP TABLE user;
