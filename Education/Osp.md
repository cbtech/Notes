# Obtenir sans punir - Christophe carré

## Chapitre 1 : Parents, un "métier" difficile

Résumé : Ce chapitre aborde les défis quotidiens auxquels les parents sont confrontés, 
         tels que les crises de colère, les refus d'obéissance et les tensions familiales.
         Il souligne la complexité du rôle parental et la nécessité de trouver des méthodes
         efficaces pour gérer ces situations.

**Actions à mettre en place :**  
  - Reconnaître et accepter les difficultés inhérentes à la parentalité.
  - Chercher activement des stratégies pour améliorer la communication et la relation avec l'enfant.

## Chapitre 2 : La manipulation, quelle manipulation ?

Résumé : L'auteur définit la notion de "manipulation positive" et distingue entre 
          manipulation négative et positive. Il explique comment la manipulation positive 
          peut être utilisée de manière éthique pour influencer le comportement des enfants sans recourir
          à la coercition.

**Actions à mettre en place :**  
  - Comprendre les principes de la manipulation positive.
  - Identifier des situations où ces techniques peuvent être appliquées de manière éthique.

## Chapitre 3 : La manipulation contre la communication ?

Résumé : Ce chapitre explore la relation entre manipulation et communication,
          en montrant comment une communication efficace peut intégrer des techniques 
          de manipulation positive pour renforcer les liens parent-enfant.

**Actions à mettre en place :**  
  - Améliorer les compétences en communication avec l'enfant.
  - Intégrer des techniques de manipulation positive pour faciliter les échanges.

## Chapitre 4 : Croyances et archaïsmes

Résumé : L'auteur examine les croyances traditionnelles et les approches archaïques en 
          matière d'éducation, en soulignant leurs limites et en proposant des alternatives 
          modernes basées sur la compréhension et le respect mutuel.

**Actions à mettre en place :**
  - Remettre en question les croyances éducatives traditionnelles.
  - Adopter des approches éducatives basées sur le respect et la compréhension.

## Chapitre 5 : Une vision et des attentes positives

Résumé : Ce chapitre met l'accent sur l'importance d'avoir une vision positive 
         de l'enfant et de formuler des attentes réalistes et encourageantes pour 
         favoriser son développement et son autonomie.

**Actions à mettre en place :**

  - Développer une vision positive des capacités de l'enfant.
  - Établir des attentes réalistes et encourageantes pour l'enfant.

## Chapitre 6 : Agir sur la relation

Résumé : L'auteur propose des stratégies pour renforcer la relation parent-enfant, 
         en insistant sur l'importance de la confiance, de l'écoute active et de l'empathie.

**Actions à mettre en place:**  

   - Pratiquer l'écoute active et montrer de l'empathie envers l'enfant.
   - Construire une relation de confiance mutuelle avec l'enfant.

## Chapitre 7 : Des outils d'influence

Résumé : Ce chapitre présente divers outils de manipulation positive, tels que le 
         sabotage bienveillant, le recadrage, l'alternative illusoire, le "pied dans la porte",
         l'étiquetage et l'inoculation d'habitudes, pour guider le comportement de l'enfant de 
         manière constructive.

**Actions à mettre en place:**  

  - Apprendre et appliquer des techniques de manipulation positive adaptées.
  - Utiliser ces outils pour influencer positivement le comportement de l'enfant.

## Chapitre 8 : L'engagement contre l'autorité

Résumé : L'auteur discute de l'importance de favoriser l'engagement volontaire de l'enfant plutôt que d'imposer une autorité stricte, en montrant comment cela conduit à une meilleure coopération et à une autonomie accrue.

**Actions à mettre en place:**  

  - Encourager l'engagement volontaire de l'enfant dans les tâches et les responsabilités.
  - Réduire l'utilisation de l'autorité coercitive au profit de la collaboration.

## Chapitre 9 : Obtenir sans imposer

Résumé : Ce chapitre synthétise les stratégies permettant d'obtenir la coopération de l'enfant sans recourir à l'imposition, en mettant l'accent sur la persuasion, la négociation et le respect des besoins de l'enfant.

**Actions à mettre en place:** 

  - Utiliser la persuasion et la négociation pour obtenir la coopération de l'enfant.
  - Respecter et prendre en compte les besoins et les désirs de l'enfant.

## Chapitre 10 : Inévitables conflits

Résumé : L'auteur reconnaît que les conflits sont inévitables dans la relation parent-enfant et propose des méthodes pour les gérer de manière constructive, en transformant les désaccords en opportunités d'apprentissage et de croissance mutuelle.

**Actions à mettre en place:**  

  - Accepter que les conflits font partie de la relation parent-enfant.
  - Appliquer des techniques de résolution de conflits pour gérer les désaccords de manière constructive.
