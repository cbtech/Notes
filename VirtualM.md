# ---- [ VIRTUAL MACHINE ] ----

## FLATPAK
    * Show application location
        flatpack info --show-location com.google.AndroidStudio
    * List application
        flatpak list
## DOCKER
  * INSTALL DOCKER ON KALI LINUX 
    apt install -y docker.io
    systemctl enable docker --now
    usermod -aG docker $USER
    docker

  * ALPINE LINUX
      service docker start
      
      - Edit /etc/group and add user to docker

## DOCKER COMMANDS
    docker  info			                            Informations about docker
    docker stop ubuntu	                        		Stop Ubuntu container
    docker rm ubuntu		                        	Remove Ubuntu
    docker rmi IMAGE                                    Remove image
    docker container rm <container_id>                  Remove container 
    docker system prune			                        Remove all containers
    docker network ls
    docker network inspect <NETWORKID>                 View Docker informations
    docker network rm <NETWORKID>

* GET SPECIFIC DISTRIBUTION (https://hub.docker.com/search?q=linux)
    docker pull ubuntu			                       Get Ubuntu
    docker run -it ubuntu 		                       Run ubuntu container 
    docker run -it alpine bash 		                   Run Alpine container with bash shell
                                                       (bash need to be installed)
    docker run -it --device=/dev/ttyACM0 ubuntu        Run ubuntu with device /ttyACM0
    docker ps -a			                           List all containers

 * BUILD IMAGE(Dockerfile) [DEPRECATED]
   docker build -t <name> .
   
   Clone the repository (example:https://github.com/jlesage/docker-firefox.git)
   /!\ don't cd to this repo
   just use buildx
   docker buildx build docker-firefox


* CREATE DOCKER FILE
  * Create docker file based on alpine with python cython installed package

 FROM alpine:3.16
 RUN apk add --no-cache python3 cython lua5.3 
 COPY file /usr/local/bin 	→ Copy file to the container 

* Build container with the following command
  docker build -t alpine .
  docker run -it alpine

* Copy files 
  - from container to computer
    docker cp <container_id>:file .

  - from computer to docker
    docker cp file <container_id>:/    → Copy to the root 

## JDOWNLOADER 
    docker run -d \ --name=jd2 \ -p 5800:5800 \ 
        -v /docker/appdata/jd2:/config:rw \ 
         -v /home/cbtech/JD2DW:/output:rw \ jdownloader

## USE TOR THROUGHT DOCKER CONTAINER
  FROM alpine:3.16
  RUN apk update && apk add \
     tor --update-cache --repository http://dl-3.alpinelinux.org/alpine/edge/testing/
     && rm -rf /var/cache/apk/*

  RUN echo "SocksPort 0.0.0.0:9150" >> /etc/torrc
  EXPOSE 9150
  CMD tor -f /etc/torrc

  - build image(file Dockerfile)
    docker build -t tor .

  - Run docker 
  	docker run -d --rm --name tor -p 9150:9150 tor
  	 
--name torproxy \
  - Configure Firefox using socks v5 127.0.0.1 port 9150
    driver failed programming external connectivity on endpoint webserver
    
    sv stop docker
    rm /var/lib/docker/network/files/local-kv.db
    sv start docker

# VIRTUAL MICHINE USING QEMU
    VM acceleration  requires hypervisor, à tool that uses virtualization provided by your cpouter processor.
    Without hypervisor and VM acceleration , the emulator must translate the machine code from VM block by block
    to conform to the architecture 

## INSTALLATION
* KALI LINUX 
	apt install qemu-kvm libvirt-clients libvirt-daemon-system bridge-utils

* ALPINE LINUX
	apk add qemu-system-x86_64
	apk add qemu-ui-gtk
	apk add qemu-ui-spice-core
	apk add qemu-hw-usb-host
	apk add qemu-hw-display-qxl

* ADD USER TO GROUP
	adduser <username> libvirt
	adduser <username> kvm

## QEMU
* CREATE NEW IMAGE SIZE 4G:
    qemu-img create -f raw <img_file> <size>
    qemu-img create -f qcow2 <img_file> <size>

* RESIZE IMAGE
    qemu-img resize <disk_image> +10G

* INFO IMAGE	
    qemu-img info void_linux-*.iso

* RUN MACHINE
    qemu-system-x86_64 <img_file>

* INSTALLING OPERATING SYSTEM
    qemu-system-x86_64 -enable-kvm -nographic -m 2G -cdrom <iso_image> -boot order=d 
		           -drive file=<img_file>,format=raw
	
* RUN WITHOUT SPECIFIC KERNEL AND INITRAMFS
    qemu-system-x86_64 -kernel /boot/vmlinuz-linux -initrd /boot/initamfs-linux -append ="root=/dev/sda" -hda myos.img

* EXIT NOGRAPHIC MODE
	ctrl+a then c
	retypr to return in virtual machine

## KEYBINDINGS
	Ctrl+Alt+m									Enable/Disable Menubar
	Alt+Shift+LeftClick					Full machine keyboard
	CTRL+ALT+G									Out of machine
	CTRL+ALT+F									FullScreen

## USB
	qemu -usb -device qemu-xhci

## GRAPHICS CARD
	1. INSTALL qemu-hw-display-qxl

## KVM(Kernel-base Virtual Machine)
	Qemu use CPU extension(HVM) instead KVM
	
 * HARDWARE SUPPORT
	1) Enable Virtualization in BIOS
	2) LC_ALL=C lscpu | grep Virtualization
	 or grep -E --color=auto 'vmx/svm/0xC0F' /proc/cpuinfo

 * KERNEL SUPPORT
		grep CONFIG_KVM /boot/config<version>

		lsmod | grep kvm
			kvm_intel		u_type1
			kvmgt				kvm
			mdev				irqbypass (/lib/modules/<kernel>/virt/lib)
			vfio
		
* TO CHECK IF KVM IS OKAY
	kvm-ok


## MONITOR(COMMAND)
* ADD USB TO GUEST OS
	device_add usb-ehci,id=ehci

* SHOW INFO ABOUT VM
	info usb 
	info network
	info kvm ...
## CONFIG OPTIONS
* KERNEL vhost-net Kernel 5.7 and later
		`Device Drivers  --->
			[*] VHOST drivers  --->
			<*>   Host kernel accelerator for virtio net`

* KERNEL	Optional advanced networking support
			`Device Drivers  --->
			  [*] Network device support  --->
	      		  [*]   Network core driver support
			  <*>   Universal TUN/TAP device driver support`

* KERNEL Enabling 802.1d Ethernet Bridging support
			`[*] Networking support  --->
        			Networking options  --->
			           <*> The IPv6 protocol
				   <*> 802.1d Ethernet Bridging`

## TROUBLESHOOTING
	Cannot setup guest memory 'pc.ram'
		This is due to -m<x>G option.reduce xG





