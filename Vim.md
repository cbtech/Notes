# [ VIM ]

    <[index.md]
    [Netwr Explorer](VIM/netrw.md)

## USEFUL NOTES
   - `st -e vim -c 'vert term /bin/bash'`    Start VIM with Term   
   - `:scriptnames`					         List scripts installed  
   - `q:`                                    Open Menu with history  
   - `vim +2244 file.cpp`                    Open VIM at line 2244

### Define endline marker
    :set colorcolumn=72
    hi colorColumn

### Man page using VIM  
   MANPAGER="vim +MANPAGER" man cp  
   MANPAGER="vim -m +MANPAGER --not-a-term -" man cp  

**Neovim**
   export MANPAGER="nvim  +Man!"

   Open manpage on programming keyboard
   put your cursor under keyword press shift+k

## XCLIP USE
```vim
   !xclip -sel c -f      	 Copy to xclip  
   :r !xclip -sel c -o      	 Paste from xclip
```

## SYNTAX highlight under cursor
```vim  
    nnoremap <F10> <cmd>echo
    \ 'hi<' .. synIDattr(           synID(line('.'), col('.'), 1),  'name') .. '> '
    \ .. 'trans<' .. synIDattr(           synID(line('.'), col('.'), 0),  'name') .. '> '
    \ ..    'lo<' .. synIDattr(synIDtrans(synID(line('.'), col('.'), 1)), 'name') .. '>'
    \<cr>
```

## DIGRAPH 
    `:digraphs`               List All digraphs codes  
    [INSERT] `<Ctrl>k  -!`    Insert ↑  
             `<Ctrl>k  <-`    Insert ←  
             `<Ctrl>k ->`     Insert →  
             `<Ctrl>k -V`     Insert ↓  


## KEYBINDINGS
```vim
   C-w + n  	    New window
   C-w + c          Close window
   gf               Open file in the same window
   C-w + f          Open in new window
   C-w + gf         Open file under cursor on new tab 
```

### Terminal
```vim
  :term /bin/bash                    Open terminal  
  :vert term /bin/bash               Open vertical Terminal  
  CTRL-W N                           Normal Mode  
  i                                  Insert Mode  
  CTRL-W c                           Close Window   
  CTRL-W q                           Quit Window  
  CTRL-W + T                         Tab to new Window
```
### Action
```vim
   xp                       Swap two characters  
   g/text/                  Show All lines with text  
   /pattern                 Search forward pattern  
   ?pattern                 Search backward pattern  
   *                        Search words under the cursor  
   [I                       Show lines with matching words  
   noh                      Remove highlighting  
   R / r                    ReplaceMode / Replace char  
   > / <                    Indent / Unindent  
   di(                      Delete everything between ()  
   Ctrl+r + (insert mode)   Paste clipboard content  
   Ctrl+v                   Visual Block  
   3p                       Paste 3 Times  
   ciw                      Change Inner word  
   di(                      Delete everything () "" '' ...  
   dw                       Del current pos to newt word  
   d$                       Delete current pos to end of line  
   d0                       Delete current pos to begin of line  
   Yiw                      Copy inner word  
   :g/^#/d                  Remove all commented lines  
   :g/^$/d                  Remove all blank lines  
   c3l                      Replace 3 char  
```
### Navigation
```vim
   h                     Move left  
   k                     Move Up  
   j                     Move Down  
   l                     Move Right  
   w                     Next word  
   e                     End of word  
   b / B                 Previous word / WORD  
   0                     Start of line  
   $                     End of line  
   gg/ G                 Begin of file / End of file  
   CTRL + ]              Jump to quickref help  
   CTRL + O              Jump older location  
   :vert help            Help in vertical mode  
   (                     Start of sentence  
   )                     End of sentence  
   {                     Start of paragraph  
   }                     End of paragraph  
   H/L/M                 Move to top/Bottom/Middle  
   %                     Next bracket  
   :N                    Goto N line  
   Ctrl + d/u            Go 1 Screen down / Go 1 Screen up  
   zj / zk               Prev / Next fold  
   gf                    Go file under cursor  
```

### Tabs / windows
```vim
   :tabnew <file>        Opening file in a new tab
   :tabnext              Next Tab
   :tabprev              Previous Tab
   :tabe <filename>      Edit file in new file
   <Ctrl+ww>                Switch window
   gt/gT/#gt             Tab Next/Prev/Tab#
```

### Search
```vim
   %s/pattern/foo        Change all pattern / foo  
   %s/pattern/foo/c      Change all pattern / foo with conf  
   %s/pattern/foo/g      Change all occurence  
   n / N                 Next Search Match /Prev Search  
```
### Marks
```vim
   m[a-z]                 Create Mark  
   '[a-z]                 Jump to Mark  
   '[A-Z]                 Jump to mark over the files  
```

### Fold
```vim
   setlocal foldmethod=expr  Fold(hide)  Expr:syntax/indent/...  
   zf                        Define Fold  
   zo                        Open fold  
   zc                        Close fold  
   zd                        Delete fold  
```

### Buffer
```vim
    :buffer <nb>              Switch to buffer <nb>  
   :ls                       List all buffer  
   :b <TAB>                  Switch between buffer with TAB  
   :bd                       Delete Buffer  
   :bw                       Write buffer and close  
```
### Other
```vim
   :htp                     Show Runtimepath  
   :ls                      List all Buffer  
   :tab h helpgrep conceal  Summarize of conceal  
   :!make -j8               execute Shell Command  
   :set nornu               Disable relative number   
```

### Help
```vim
   :tab h pattern        Open help in new tab  
   :vert h pattern       Open help in vertical split  
   :tab h CTRL-W         View all CTRL-W Shortcuts  
```

### Navigate throught help(tags)
  Move cursor to the bar, then **CTRL+]** 
  to go to the selected selection

## BUFFERS
```vim
  :ls		List buffer  
  :bd		Delete buffer  
  :bw		Delete buffer  
```

### Search using vimgrep
   - Go to the location of current edited file  
      `:cd %:h`
   - Search sed occurrence in file and open occurence highlight  
      `vimgrep "sed" file.cpp | copen`  
   - search sed recursively  
      `vimgrep "sed" **/*.md`  
   - Search in the current filepath active buffer  
   	   `vimgrep "lambda" %`  

### Macro
```vim
   Press q<letter>                        Record macro letter   
   <number>@<letter>                      Play <number> times macro  
   @<letter>                              Execute macro  
   @@                                     Execute macro again  
```

## PRINTING
  1. Define Font and Print in ps
     `set printfont=CodeNewRomandNerdFont:h12`  
     `:ha > file.ps`
 
  2. Transform file into PDF  
      `ps2pdf file.ps` 


