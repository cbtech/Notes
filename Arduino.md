# ARDUINO 

## LESSONS
   [01_Internal LED](Arduino/01_internalLed.md)  
   [02_External LED](Arduino/02_externalLed.md)  
   [03_RGB LED](Arduino/03_RGBLed.md)  
   [05_Button](Arduino/05_digitalInput[Btn].md)  
   [06_ActiveBuzzer](Arduino/06_activeBuzzer.md)  
   [07_PassievBuzzer](Arduino/07_passiveBuzzer.md)  
   [08_TiltBallSwitch](Arduino/08_tiltBallSwitch.md)  
   [09_ServoMotor](Arduino/09_Servomotor.md)  
   [10_UltrasonicSensorModule](Arduino/10_ultrasonicSensorModule.md)  
   [11_Membrane_switch_module](Arduino/11_MembraneSwitchModule.md)  
   [12_HumiditySensor](Arduino/12_DHT11TemperatureHumiditySensor.md)  
   [13_AnalogJoystickModule](Arduino/13_AnalogJoystickModule.md)  
   [14_IRReceiverModule](Arduino/14_IRReceiverModule.md)  
   [15_LedDotMatrix](Arduino/15_LedDotMatrix.md)  
   [16_IMUAccelerometer_Gyroscope](Arduino/16_IMUAccelerometer_Gyroscope.md)  
   [17_PIRMovementDetection](Arduino/17_PIRMovementDetector.md)  
   [18_WaterlevelDetection](Arduino/18_WaterlevelDetection.md)  
   [19_RealtimeClockModule](Arduino/19_RealtimeClockModule.md)  
   [20_SoundSensor](Arduino/20_soundSensorModule.md)  
   [21_RC522RFID](Arduino/21_RC522RFIDModule.md)  
   [22_LCDDisplay](Arduino/22_LCDDisplay.md)  
   [23_Thermometer](Arduino/23_thermometer.md)  
   [24_8Ledwith74HC595](Arduino/24_8LEDwith74HC595.md)  
   [25_SerialMonitor](Arduino/25_SerialMonitor.md)  
   [26_PhotoCell](Arduino/26_photocell.md)  
   [27_74GC595_SegmentDisplay](Arduino/27_74GC595SegmentDisplay.md)  
   [28_4DigitalSegmentDisplay](Arduino/28_4DigitalSevenSegmentDisplay.md)  
   [29_DCMotor](Arduino/29_DCMotor.md)  
   [30_Relay](Arduino/30_relay.md)  
   [31_StepperMotor](Arduino/31_Steppermotor.md)  
   [32_StepperMotorwithRemote](Arduino/32_SteppermotorwithRemotecontrol.md)  

   ![led_diagram](Arduino/img/led_wiring.png)

## Commands
    arduino-cli board list
    arduino-cli core update-index
    arduino-cli core list
    arduino-cli core search
    arduino-cli core install arduino:avr
    arduino-cli sketch new BlinkMe

## Core
https://github.com/arduino/ArduinoCore-avr

# Build sketch
   arduino-cli compile -b arduino:avr:mega -v --output-dir .

**Build using directory**  
      Create *Serial_monitoring* with *Serial_monitoring.ino*. And build using this command:  
      `./arduino-cli_1.0.4 compile -b arduino:avr:mega /home/cbtech/Arduino/Serial_monitoring -v --output-dir .`
        
**Using avr-gcc**\
```c
avr-gcc -c -Os -mmcu=atmega2560 -DF_CPU=16000000UL -c led.c -o led.o\
avr-gcc -mmcu=atmega2560 led.o -o led.elf\
avr-objcopy -O ihex -j .data -j .text led.elf led.hex\
```    

## Upload to board
  arduino-cli upload -b arduino:avr:mega -p /dev/ttyACM0 -v
     
**Using avrdude**   
- `avrdude -c ?`         → Show programmer options  
- `avrdude -p ?`         → Show partno AVR device  

- `avrdude -v -patmega2560 -P/dev/ttyACM0 -cwiring -b115200 -D -Uflash:w:led.hex:i`

# LOWLEVEL 

## HARDWARE
   A register consists of 8 bits in the AVR microcontroller(called REGISTERS or PORTS).
    The bits are counted from **Right** to **Left**.Bit 7 is also known as the Most Significant Bit(MSB)
    Bit 0 is known as the Least Significant Bit(LSB). The registers are normally accessed byte-by-byte.

 Example of DDRB7(The PORT B Data Direction Register
Bit     7       6       5       4       3       2       1   
     DDRB7    DDRB6   DDRB5   DDRB4   DDRB3   DDRB2   DDRB1


