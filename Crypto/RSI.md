# [ RSI - Relative Strength Index]

   RSI measures the speed and the magnitude of a security's
   recent price changes to detect overbrought and oversold

   - overbrought when **RSI is above 70**
   - oversold when **RSI is below 30**

## RSI INVESTOPIA
|   Day       |    ClosingPrice      | Daily change  |
|-------------|----------------------|---------------| 
|    1        |       100            |    ---        |                 
|    2        |       100.1          |   +0.1        |
|    3        |       100.05         |   -0.05       | 
|    4        |       100.15         |   +0.1        |
|    5        |       100.02         |   -0.13       |
|    6        |       100.25         |   +0.23       |
|    7        |       100.15         |   -0.1        |
|    8        |       100.3          |   +0.15       |
|    9        |       100.4          |   +0.1        |
|    10       |       100.22         |   -0.18       |
|    11       |       100.28         |   +0.06       |
|    12       |       100.24         |   -0.04       |
|    13       |       100.32         |   +0.08       |
|    14       |       100.35         |   +0.03       |

**Total Gains**  = 0.1 + 0.1 + 0.23 + 0.15 + 0.1 + 0.06 + 0.08 + 0.03 = 1.00(1% of initial Price)  
**Total Losses** = 0.05 + 0.13 + 0.1 + 0.18 + 0.04 = 0.8 (0.8% of initial Price)  



