# Backtesting using backtrader

Every algorithmic trading rookie starts to wonder what backtesting is and how it is implemented. Many people overlook it based on their individual biases. However, it is a crucial step in building your algorithmic trading robot.

To put it simply, your idea or strategy can be great in your head, but data never lies. Backtesting provides an indication of whether your system is likely to work or not.

For example, when deciding which mutual fund or PMS service to invest in, you often look at 3-year or 5-year returns to make a decision because data speaks for itself.

Let's start learning the backtesting framework by creating and backtesting a simple strategy. We will demonstrate a straightforward strategy to introduce the library; real-world strategies are much more complex and require various other factors to be considered. This article is aimed at beginners.

## Our Sample Strategy

1. **Quantity**: 100  
2. **Start Cash**: 100,000  
3. **Commission**: 0.2%  
4. **Position**: Long  
5. **Frequency**: Daily  
6. **Start Date**: 1st Oct, 2021  
7. **End Date**: 15th Nov, 2021  
8. **Buy Condition**: When 21-period RSI crosses above 30 and 50-period SMA crosses above 100-period SMA  
9. **Sell Condition**: When 21-period RSI crosses below 70  

## Installing Required Libraries

```bash
pip install backtrader yfinance
```

## Importing the Libraries

```python
import backtrader as bt
import yfinance as yf
from datetime import datetime
```

## Creating a Class to Define Our Strategy

```python
class FirstStrategy(bt.Strategy):
    def __init__(self):
        self.rsi = bt.indicators.RSI(self.data.close, period=21)
        self.fast_sma = bt.indicators.SMA(self.data.close, period=50)
        self.slow_sma = bt.indicators.SMA(self.data.close, period=100)
        self.crossup = bt.ind.CrossUp(self.fast_sma, self.slow_sma)

    def next(self):
        if not self.position:
            if self.rsi > 30 and self.fast_sma > self.slow_sma:
                self.buy(size=100)
        else:
            if self.rsi < 70:
                self.sell(size=100)
```

## Setting Up Backtesting Environment

```python
startcash = 100000
cerebro = bt.Cerebro()
cerebro.addstrategy(FirstStrategy)
```

## Loading Data [OK]

```python
data = bt.feeds.YahooFinanceCSVData(
    dataname="HDFCBANK.NS.csv",
    fromdate=datetime(2020, 11, 1),
    todate=datetime(2021, 11, 1))
cerebro.adddata(data)
```

## Configuring Broker

```python
cerebro.broker.setcommission(commission=0.002)
cerebro.broker.setcash(startcash)
```

## Running Backtest

```python
cerebro.run()
portvalue = cerebro.broker.getvalue()
pnl = portvalue - startcash
print(f'Final Portfolio Value: ${portvalue:.2f}')
print(f'P/L: ${pnl:.2f}')
```

## Expected Output

```
Starting Portfolio Value: $100000.00
Final Portfolio Value: $100000.00
P/L: $0.00
```

This guide gives a basic introduction to backtesting using `backtrader`. Further refinements and optimizations are necessary for real-world applications.



