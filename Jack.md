# ----[ JACKD ]--- 
                        
    [<GO Back>](index)
## OUTPUT TO TTY
 JACK_NO_AUDIO_RESERVATION=1 jackd -d alsa -Phw:1

## OUTPUT TO HDMI TV
    aplay -l 
        output →   card 1: HDMI [HDA ATI HDMI], device 9: HDMI 3 [SAMSUNG]
                    
    jackd -d alsa -Phw:1,9 or jackd -d alsa -Phw:CARD=HDMI,3
    
    mpv -ao jack music.mp3
    if you haven't sound please check if spdif of HDMI output is not mute in alsa mixer

## CONNECT TUXGUITAR THROUGHT FLUIDSYNTH
    xbps-install -S fluidsynth soundfont-fluid

    fluidsynth --server --audio-driver=jack 
            --connect-jack-outputs /usr/share/soundfonts/FluidR3_GM.sf2
    
    Connect fluidsynth to output thought qjackctl
    In tuxguitar:
        Midi sequencer: use tuxguitar sequencer
        Mid Port: Synth input port(1568:0)[128:0]

## CONNECT INPUT TO OUTPUT
    Install njconnect
        Press [a] to manage audio
        Connect output port to input with [Enter] or [c]
        Disconnect with [d]
        [?] Help 

## CONNECTION WITH SAMSUNG / ROCKSMITH GUITAR USB
	dbus-launch  jackd -d alsa -C hw:2 -Phw:1

## ADD LIMITS → man limits.conf
 Add user to audio group
 * Edit /etc/security/limits.d/jack.conf, add this two lines:
  	@audio - rtprio 99
	@audio - memlock unlimited
 
Check the contents of RT_SCHEDULING
  grep 'RT_GROUP_SCHED' config-5.4.10_2
  RT_GROUP_SCHED 	


