# [ Raspberry PI ]

## Create swap under libreElec
   cp /etc/swap.conf /storage/.config  
   SWAP_ENABLED="yes"  
   SWAPFILESIZE="128" (The size of RAM must be 4x RAM) 

## Modify the video caching
   The cache settings are now adjusted in the Settings ► Services ► Caching settings page. 
