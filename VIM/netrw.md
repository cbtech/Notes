# Vim explorer NetRW


## Commands
 :Sex Open Explore in split  
 :Sex! Open Explore in Vertical Split   
 :e%:h/filename Edit file in current dir  
 :Texplore Open Explorer in new tab  
 :w%:h/filename Write file in current dir   
 d Create Dir in explorer  
 % Create File in explorer  
 mb Mark Current directory  
 gb Go to marked directory  
 i Change List view  
 I Toggle the banner  
 t Open file in new tab  
 o Open file in hsplit  
 v Open file in vsplit  

## Change directory in Lexplore 
 After opening LExplore and navigate throught different directory, 
 this command can update the location to the current directory
     :cd %:p:h

## BOOKMARKS 
  mb Bookmark current directory  
  qb List bookmarked directory and history  

## NETRW CONFIG

let g:netrw_winsize = 12
let g:netrw_browse_split = 2

