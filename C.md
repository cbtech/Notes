# ---- [ C99 PROGRAMMING LANGAGE] ----

	<[index.md]

[../SNIPS/C/README.md] ( Examples)

## C11 
   [C11/Arrays.md] ()
   [C11/Basics.md] (Printf/Scanf)
   [C11/Bitwize.md](Bit Operation)
   [C11/Build.md] (Makefile / Libs / Linker)
   [C11/Errors.md] (Troubleshooting/Errno/Valgrind)
   [C11/File.md](fread)
   [C11/Glossary.md] ()
   [C11/Keyboard.md](Keyboard/input event/showkey)
   [C11/Memory_Allocation.md](Malloc/calloc/realoc)
   [C11/Pointers.md] ()
   [C11/Struct.md] ()
   [C11/Strings.md](String)

## Refs
    IPC Communications
        Practical C Programming - Solutions for modern C Developers (P.254)
        Pratical System Programming with C (P.195)
        The LinuxProgrammingInterface(P.1102)
        C Programming Cookbook (P.224)

## COMMENTS
    comment big part with lots of // /* */

 use #if 0
      	<some code>
     #endif

## MULTIPLE EXAMPLES IN SAME FILE
```c
	#if defined(EXAMPLE_1)
		printf("Example 1\n");
	#elif defined(EXAMPLE_2)
		printf("Example2\n");
	#endif
```	
	* BUILD: cc -g -Wall -w -DEXAMPLE_1 -o EXE

## SYMBOLIC CONSTANTS
  #define LOWER 0
