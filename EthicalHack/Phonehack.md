# ---- [ IPHONE/IPAD HACK]

				<[../index.md]

	DFU:		Device Firmware Update
## BOOKS ON IPAD
    Put books WEBSITE/Books folder
    Create a page books.html
    <a href="/Books/Python Notes for Professionals (GoalKicker.com) (Z-Library).pdf">Book7</a>
    and download on Ipad

## JAILBREAK
   * checkra1n													https://checkra.in/
	 * ipadlinux													https://ipadlinux.org/
	 * IphoneWIKI													https://www.theiphonewiki.com/wiki
	 * rera1n																			Downgrade IOS tools for Linux
	 * Uncover Jailbreak													https://unc0ver.dev
	 * Phoenix jailbreak(CydiaImpactor)						https://phoenixpwn.com
   * spirit linux

## IPOD JAILBREAK
Info: Ipod a1288 2e seconde gen xb533 
    1. connect it using usbmuxd and ifuse
 * Generate HashInfoFile
    lsusb -v | grep -i Serial
        9000275272783B43
    
    https://ihash.marcan.st/

## CYDIA IMPACTOR
	* To open cydia, You need to install libatomic1

## RERA1N							DOWNGRADE IOS TOOL FOR LINUX

 - [X] libplist		git clone https://github.com/libimobiledevice/libplist
					Read write Apple property List file in binary or XML.
 
 - [X] libusbmuxd 	git clone https://github.com/libimobiledevice/libusbmuxd
					Library to handle usbmux protocol with IOS device.

 - [] not-secret-secret	git clone https://github.com/AidanGamzer/not-secret-secret.git
 
 - [] iphonessh	  git clone https://github.com/rcg4u/iphonessh
				  Using iphone over ssh usb

 - [X] libimobiledevice		git clone https://github.com/libimobiledevice/idevicerestore
		 		        	Library to communicate with services on IOS device using native protocol.
 - [X] libimobildevice-glue	  https://github.com/libimobiledevice/libimobiledevice-glue.git
 - [ ] libirecovery				git clone https://github.com/libimobiledevice/libirecovery
 - [ ] idevicerestore	    git clone https://github.com/libimobiledevice/idevicerestore

## UNZIP IPA FILES
    rename .ipa files to .zip
    unzip myzipfile.zip -d MyApp

## MOUNT IPOD TOUCH USING IFUSE
	* INSTALL PKGS:
		fuse3 fuse3-dev
		libimobiledeive/iFuse 
		usbmuxd

		modprobe fuse
		usbmuxd
		ifuse /mnt
 * if the device is plugged and not connected, Kill al instance of 
   usbmuxd and restart with:
      usbmux -f -v 
      2. ifuse ~/IPAD

## AFC (Apple File conduit)
  is a service that runs on every iphone/ipad which itunes uses to exchange files with device.
  The afc service ishandled by  /usr/lib/libexec/afcd, and runs over usbmuxd protocol.
  
## DOCUMENTATION
	 git clone https://github.com/checkra1n/pongoOS
	 git clone https://github.com/Siguza/ios-resources

