# ---- [ GLOSSARY ] ----


## CC FLAGS
   
  -shared: Create a shared library

  -fPIC: Position independent code means
    that the generated machine code is not 
    dependent on being located at a specific
    address in order to work.

    If your code is compiled with fPIC
    it's suitable(Il convient) for inclusion in a library.
    The library must ben able to relocated from its 
    preferred location in memory to another address

## SYSTEM CALLS
IOCTL control device : 

    The ioctl system call manipulates the 
    underlying device parameters of special file.

## EVENT-TYPE:
  EV_SYN: used as markers t separate events. Events may
  be separated in time or in space, 

  EV_KEY:
    Used to describe state changes of keyboards,button, 
   
   SYN_REPORT:
     Used to synchronize and separate events into packets
     of input data changes occuring at the same moment in time.

