## ----- [C99 - BIT OPERATIONS] ----

## EXAMPLES

[../../SNIPS/C/bitwise_operation.c]
[../../SNIPS/C/bit_manipulation.c]

## LESSONS
	0x80 → 10000000
	0x7f → 11111111

  ~ (Bitwise NOT)
     takes one number and inverts all bits of it.
	Example: ~(ICANON)

  & (bitwise AND)				takes two numers as operands and does AND on every bits
     of two numbers.The result of AND is 1 only if both bits
     are 1.

        Example:          00000010
		       &  00000001
		       =  00000000 

  | (Bitwise OR)
     takes two numbers as operands and does OR on every bit
     of two numbers.The result of OR is 1 if any of the two
     bits is 1.
 	Example:	00000010
                      | 00000001
		      = 00000011
  ^ (Bitwise XOR)
     takes two numbers as operands and does XOR on every bit
     of two numbers.The result of XOR is 1 if the two bits are 
     different.

   << (Left shift)
       takes two numbers, left shifts the bits of the firs operand, 
       the second operand decides the number of places to shift.

   >> (Right shift)
      takes two numbers, right shifts the bits of the first operand,
      the second operand decides the numbers of places to shift

   * The left-sift and right-shift operators are equivalent 
      to multiplication and division by 2.

bit1   /   bit2 / ~bit1 / bit1 & bit2 / bit1 | bit2 / bit1 ^ bit2
  0	    0	    1          0	     0		   0	
  0	    1	    1          0             1             1
  1 	    0       0          0             1             1
  1         1       0          1             1             0

## Binary Array
1 00000001 
2 00000010
3 00000011
4 00000100
5 00000101
6 00000110
7 00000111
8 00001000
9 00001001

