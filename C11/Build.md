# ---- [ MAKEFILE] ----

	<[index.md]
 
 ## EXAMPLES
    [../../SNIPS/Build/Makefile2.c]

## EXCLUDE C FILE FROM MAKEFILE
SRC_FILES := $(wildcard src/*.c)
SRC_FILES := $(filter-out src/file.c, $(SRC_FILES))

# UNDEFINED REFERENCE
    Function not shown when ld is linking
    precise a correct library file.

    -L is the path to the lib 
    -l is the lib name 
        example: -lwlroots → libwlroots.so

## PASS OPTION TO COMMANDLINE
   cc -shared -fPIC *.c -DBM_VERSION="command"

# MANAGE A DEFINE
 * Add -DOPTION
	CFLAGS= -D_GLFW_WAYLAND
```c
	if defined(_GLFW_COCOA)
		#include "blah.h"
	elif defined(_GLFW_WAYLAND)
```
## CREATE LIBRARY
  * CREATE FILE .o
    gcc -c add.c
    gcc -c sub.c
  
  * CREATE LIB
  	c:create r: for replace s:for index
    ar crs libbasic add.o sub.o

## ADD OBJECT FILE TO GCC
    gcc -g -Wall menu.c lib.o -o ~/EXEC/MENU

## INSTALL DIRECTORY
 * MANPAGES: 
 	install 

install:
 	install ${EXE} ${DESTDIR}
	make install DESTDIR=/usr/bin
	
## TROUBLESHOOTING
    - Change soace to tab
    Makefile:line: *** missing separator. Stop
