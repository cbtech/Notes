# ---- [ BASICS GCC ] ----

## READ CMD LINE ARG
  arcg is the number of element
  argv is a pointer array which points to each argument passed to the program

  Example:  if(argc ==2) printf("Arg: %s \n", argv[1]);
  ```c 
  	for(int i =1; i < argc; i++)
		printf("ARG: %s", argv[i])

  * COMPARE ARGV AND STRING
   #include <string.h>

   if(!strcmp(argv[1],"hello") 
	   print("foo\n");

## SCANF 
  char key;
  scanf ("%c",key);

## PRINTF
 printf("%d", value)
 %2d        →   Timer format 09 instead of 9
 %d 		→ 	decimal integer
 %6d 		→	decimal integer , 6 character wide
 %f 		→	floating point
 %6f		→ 	floating , - char wide
 %.2f		→ 	floating point,2 char decimal point
 %6.2f		→	floating, 6 char wide and 2 after decimal point
 %o/%x/%c/%s	→ 	Octal,Hexdecimal,Character,String

## FOR LOOP
```c
	for (int i=0; i<= max; i++) {}
```

