# ---- [ KEYB PROGRAMMING / INPUT DEVICE PROGRAMMING ] ----

# Examples: 
   [../../GITHUB/TERM_APP/ptable/keyb.c](Periodic table 
            github project with just std lib)

## READ FOR KEYBOARD INPUT 
```c
int c;

while (( c = getchar()) != EOF) {
	putchar(c);
	}
```

* MAP KEY
  #define CTRL_KEY(k) ((k) & 0x1f)
  if (c == CTRL_KEY('q')) break;

 * GET STRINGS FROM INPUT
 char *str = malloc(256);

 fgets(str,sizeof(str),stdin);

## READ FILE (cat /dev/input/event10), 
	libinput debug-events to show keycode and hardware plugged.
	Show some weird characters.

## input_event struct(/usr/include/linux/input.h)
 contains 3 fields (type,code,value)

 All events type are listed in this files.
 All documentation about event codes are described 
 in the linux kernel → documentation/input/event-codes.rst

