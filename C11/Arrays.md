# ---- [ ARRAYS ] ----

## ARRAY
 * CLEAR ARRAY
     arr[0]  = '\0';

 * SEND ARRAY TO FUNCTION
    - by individual elements

```c
 void display(int age1, int age2)
    ...
 
 int main() { 
	 int age_array[]={2,8,4,12};
	 display(age_array[1],age_array[2]);

      // Send entire array
        float calculateSum(float[] num){
	    float sum = 0.0:

	  for(int i=0; i < 6;++i) {
		  sum +=num[i];
	  }
	return sum; 
  }

  int main() {
	  float result, num[] = { 23.4, 55,22.6};
	  result= calculateSum(num);
  }
```

