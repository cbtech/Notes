# ---- [ FRAMEBUFFER C PROGRAMMING] -----

# RESOURCES
    http://betteros.org/tut/graphics1.php

# INFORMATIONS
	cat /sys/class/graphics/fb0/modes	
    
    Lot's of programming is located  in:
        kernel/drivers/video/fbdev/core/fbcon.c

    Font and setvtrgb is located under the kbd package.

## STTY
    * Show information about terminal 
            stty -a >> log

## FBCONSOLE(fbcon)
    The framebuffer console is a text console running on top of the framebuffer
    device.

    (Kernel)>Device Driver->Graphics Support->Framebuffer Device->Console display
    driver support ->Framebuffr Console Support
    
## FBDEV PROGRAMMING(BETTEROS ARTICLE)
Get information using FBIOGET_VSCREENINFO, and chenge settings using
FBIOPUT_VSCREENINFO
  * Example 
        ioctl(fb_fd,FBIOGET_VSCREENINFO,&vinfo);
        vinfo.grayscale=0;
        vinfo.bits_per_pixel=32;
        ioctl(fb_fd, FBIOPUT_VSCREENINFO,&vinfo)
    
    
  * Size of the screen
        long screensize = vinfo.yres_virtual * finfo.line_length;
  
  * Map the buffer into memory     
        uint8_t *fbp = mmap(0,screensize,PROT_READ | PROT_WRITE, MAP_SHARED,
        fb_fd,(off_t) 0);

  * Calculate the correct location in the mapper memory of the pixel that we
    want to set.

      long x,y;
      uint32_t pixel;
      long location = (x + vinfo.xoffset) * (vinfo.bits_per_pixel/8) +(y +
      vinfo.yoffset) * finfo.line_length;
      *((unit32_t*)(fbp + location)) = pixel

 (y + vinfo.yoffset) * finfo.line_length → Get the begining of line y in
                                                    memory 

 (x + vinfo.xoffset) *(vinfo.bits_per_pixel/8) → Get the offset of x on that line

 * little function that takes 8bit values for each color and 
   returns the pixel values
        inline uint32_t pixel_color(uint8_t r, uint8_t g, uint8_t b,struct
                                    fb_var_screeninfo *vinfo)
        return (r << vinfo->red.offset) | (g << vinfo->green.offset) | 
               (b << vinfo->blue.offset);
    
    This function takes the 8 bit value and shifts it to the left the correct
    offset of that color.Then combines it with the other color using the OR
    operator(|).So if we want to draw a pixel of color 0xFF,0x00,0xFF(Purple), 
    it take the red value(0xFF) shift it over the correct offset or red(probably
    16) and the result would be 0x00FF0000.

    ! Don't draw outside the screen(SEGFAULT)
        To make sure we don't draw outside the screen, we can use
        vinfo.xres(width) and vinfo.yres.

## DRM KERNEL DEBUGGING
    ls /sys/class/drm/card0
    ls /sys/kernel/debug/dri/0
    cat $(readlink -f /sys/class/graphic/fb0/name)	

## FRAMEBUFFER
  * Set framebuffer
	fbset -g 1024 600 1024 600 8
  
  * Get info about framebuffer
  	fbset -i 

font programming
https://newluhux.github.io/2022-09-28-linux-framebuffer-programming.html


## COLORMAP
    The colormap is simple table that contains colors

 * Get the current colormap
    
    cat /sys/module/vt/parameters/default_* >> FILE

    BLUE  53,72,93,113,134,153,174,194,137,197,136,2,190,173,137,72
    GREEN 43,64,85,106,128,149,170,191,24,109,130,0,160,14,24,124
    RED   40,61,83,104,126,147,169,190,178,120,67,223,0,121,178,199
    
    color0(Black) = 404353 = (40 /16 = 2.5)  (40 % 16 = 8) 282B35
                    43 = (43 /16 = 2.68) (43 % 16 =11(B)) 2B
                    53 = (53 /16 = 3.31) (53 % 16 = 5) 35
    color1(Red) = 726461
    color2(Green) = 938583
    color3(Yellow) = 113106104
    color4(Blue) =
    color5(Magenta) =
    color6(Cyan) =
    color7(White) =
    color8(BrightBlack) =
    color9(BrightRed) =
    color10(BrightGreen) =
    color11(BrightYellow) = 
    color12(BrightBlue) = 
    color13(BrightMagenta) =
    color14(BrightCyan) = 
    color15(BrightWhite) =


* Default color palette under Linux
	    color0	color1	color2	color3	color4	color5	color6	color7	color8	color9	color10	color11	color12	color13	color14	color15
   
[RED]   0,      170,    0,      170,    0,      170,    0,      170,    85,     255,    85,     255,    85,     255,    85,     255
[GREEN] 0,      0,      170,    85,     0,      0,      170,    170,    85,     85,     255,    255,    85,     85,     255,    255
[BLUE]  0,      0,      0,      0,      170,    170,    170,    170,    85,     85,     85,     85,     255,    255,    255,    255
   

