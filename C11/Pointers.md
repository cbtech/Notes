# ---- [C Langage - Pointers ] ----

    [../../SNIPS/C/pointers.c](Pointers / Double/Triple use examples)
    
    - Pointers are more efficient in handling arrays and structures
    - Pointers are used to return multiple values from a function
    - Pointer allows dynamic memory allocation a deallocation
      (create and deletion of variables at runtime)
    - Pointer allows to refer and pass a function as a parameter to function

## POINTER TO POINTER

    *ptr[0x1230] ------>    num(10) [0x1230]- 

    **dPtr[0x1220] ------> *ptr[0x1230] -----> num(10) [0x1230] 

    *Example* :
     int num =10;
     int *ptr = &num;
     int **dPtr = &ptr;

     printf ("Addr of ptr: %d ", dPtr);
     printf ("Addr of dPtr: %d ", *dPtr);
     printf ("Value of num: %d ", **dPtr);


## PARSE POINTER TO FIND OCCURENCE
  for ( ;*path != '\0'; path++){
    if(*path == '.') 
      // the three next letter are the ext
  }

# PRINT ADDRESS:
  printf("Adress : %p, Data: %s, Prev: %p,Next: %p\n",
      node,node->data,node->prev,node->next);

## POINTERS
```c
	char str_a[20];
	char *pointer;
	char pointer2

	strcpy(str_a,"Hello World!\n");
	pointer = str_a;
	printf(pointer);		// Hello World!
	pointer2 = pointer + 2
	printf(pointer2)		//llo World!
`
