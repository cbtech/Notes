# ---- [ ERRORS HANDLING ] ----


## VALGRIND
 valgrind --leak-check=full --show-leak-kinds=all --tracks-origins=yes --verbose --log-file=valgrind.log ./EXECUTABLE

## ERRNO
	#include <errno.h>
	if(ioctl(fd,FBIOGETCMAP,cmap) == -1) {
		fprintf("Error FBIOGETCMAP: %s\n",strerror(errno));
	}

 Or
    cnt = read(fd,buf,numbytes);
    if(cnt == -1) {
        if(errno == EINTR)
            fprintf(stderr,"Read interrupted\n");
        else {
            // etc
            }
        }
 * Error on pointer  
        char *fbp
        fbp = mmap(NULL, 1920 * 1080 * 32, PROT_READ | PROT_WRITE,
                   MAP_SHARED, fbfd, 0);

        if(fbp == MAP_FAILED) 
            printf("Error\n");
    Read manpage of mmap

## TROUBLESHOOTING
	* Cannot find lssp-nonshared
		
	  gcc -v 2>&1 | grep -- '--disable-libssp'
	  gcc is build with --disable-ssp-nonshared.Recompile gcc or install clang


