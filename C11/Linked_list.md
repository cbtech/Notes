# ---- [ LINKED_LIST ] ----

https://stackoverflow.com/questions/7271647/what-is-the-reason-for-using-a-double-pointer-when-adding-a-node-in-a-linked-lis

## SIMPLE EXAMPLE OF LOOPING THROUGHT LINKED LIST
        static void *head;

        struct linked_list *list1 = NULL;
        list1 = malloc(sizeof(struct linked_list));

        list1 = add_item(list1,1,"Yohan Melanche");
        // head points to the first element of my list
        head = list1;
        
        void draw_menu(struct screen *s,struct linked_list *list) {
            int pos_x = 15,pos_y = 10,i=0;
            
            for(list = head; list != NULL; list = list->next ) {
                  set_position(pos_x,pos_y++); 
                  printf("%d. %s" ,i++, list->data);
            }
        }

## FIRST CASE DOUBLE POINTER
struct node* push(struct node **head, int data)
{
        struct node* newnode = malloc(sizeof(struct node));
        newnode->data = data;
        newnode->next = *head;
        *head = newnode // *head store the newnode in the head 
        // return newnode;
}
push(&head,1);

## SECOND CASE
 The implementation that doesn't take a pointer to the head pointer must return the new head
 and the caller is responsible for updating it itself.
struct node* push(struct node *head, int data)
{
        struct node* newnode = malloc(sizeof(struct node));
        newnode->data = data;
        newnode->next = head;
        return newnode;
}

head = push(head,1)
