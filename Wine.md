# -------------------- [ WINE ] --------------------------

Monkey island Sound device failed or not found 

    pamac install lib32-libpulse

    WINEPREFIX=~/.wine32 WINEARCH=win32 winecfg
    WINEPREFIX=~/.wine32 winecfg

    WINEPREFIX=~/.wine32_clean winetricks xact
    WINEPREFIX=~/.wine32_clean winetricks d3dx9
    WINEPREFIX=~/.wine32_clean winetricks d3dcompiler_47

Other solution 
    
    ~/.wine/drive_c/Program Files (x86)/Lucasarts/The Secret of Monkey Island Special Edition/
    
    Edit or Create Settings.ini
    [display]
    windowed=1
    resolution=1920x1080
