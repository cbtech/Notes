# ----- [ TRADING ] ----

	<[../index.md]

## KLINE(Candlestick chart):
    is a type of financial chart used to represent price movement of an asset.

    Each K-LINE represetn a specific time period (day, hour,minute) and provides
    the following key information
        1.Open: The price at which the asset  started trading  at the begining 
                of the time period
        2. High: The highest price reached during the time period
        3. Low: The lowest price reached during the time period
        4. Close: The price at which the asset ended trading at the end of the time period.
    

## BLOCKCHAIN
	Blockchain is a system thats check is 
	everything is correct.

	  - A distributed database or ledger.
	  - Keep tracks of all balances and transactions.
	  - Peer to peer network.
	  - Protected by cryptographic(Security and data integrity).


  * CANDLESTICK
	
	RED:			GREEN:
	     | High 		        | High
         █ Open               	█ Close
       	 █ Close              	█ Open 
         | Low                	| Low
        
      
      The bigger the body, more intense the buying/selling pressure

 * HOW TRANSACTIONS WORKS:
    1. You gather up(rassembler) informations from the blockchain
    	about the funds you're going to send, put toghether a transaction
   	and add digital signature to it.

    2. The digital signature is essential.They're the only way you can prove
        that you are the owner of the funds. and have the right to spend.
	 
	 If you use and exchange, the exchange handles it for you.
	 If you use a mobile wallet, it happens behind the scenes, when you make a transaction.

    3. Other nodes compare your signatures with you public key to validate
        your transaction.

## TRADING WITH MA(Moving Average)
	Moving Average is simply a line that represents the average value
	of previous data during predefined period(12d)
	  SMA: Simple Moving Average
	  EMA: Exponential Moving Average

## TRADING WITH MACD(Movine Average Convergence / Divergence)
	Devellopped Gerald Appel 1970.
	MACD line is calculate by substract 12d EMA - 26d EMA.
