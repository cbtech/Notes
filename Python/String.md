# ---- [ PYTHON STRING] ----

        < [../index.md]
    [../../SNIPS/PYTHON/] (Examples)

## LOOP THROUGH STRING
    i = 0
    while(i != len(str)):
        print("[generateAcronym]: ",str[i])
        if(str[i] == ' '):
            print("Ooops, It's a Space")
        i += 1

## INVERT STR 
     str='Hello my name is Jack'
    print(str[::-1])	kcaJ si eman ym olleH

## REMOVE SUBSTRING FROM STRING
```python
       my_string.replace('occurence', 'remplacement')
```
