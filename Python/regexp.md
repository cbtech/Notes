# ---- [ REGULAR EXPRESSION ] ----


# RESOURCES 
  https://regex101.com/ 	→ Regexp online interpreter
  https://regexr.com/		→ Useful regexp interpreter
  https://www.regular-expressions.info
  https://realpython.com/regex-python/
  https://ryanstutorials.net/regular-expressions-tutorial/regular-expressions-examples.php#html
  https://www.programiz.com/python-programming/regex
  https://exploringjs.com/es2018-es2019/ch_regexp-named-capture-groups.html#named-capture-groups
  https://medium.com/factory-mind/regex-tutorial-a-simple-cheatsheet-by-examples-649dc1c3f285

[0-9]			Matches single digit between 0-9
[0-9][0-9]		Matches 2 digits between 0-9
[0-9a-fA-F]		Matches single hexedecimal digit
(?P<server>[^"]*) 	Capture everythin in " "

 * Uppercase is opposite.
 + indicates that the number can contain a minimum of 1
   or maximum any number of digits.

\0   Matches nul char.
\b   Matches at the beginning or end of a word.
\d   Matches single character that is a digit.
\w   Matches word character Example: #(.a$@& → return a.
\r   Matches carriage return characters.
\t   Matche horizontal tabs.
\s   Matches any whitespaces.			

## ANCHORS
\A ^ 	Match to the start of the string
\Z $	Match to the end of the string

 ? 	Matches zero or one repetitions of the preceding regex.


## FIND DATE
date = '1956-02-04'
re.findall(r'([0-9]{4})-([0-9]{2})-([0-9]{2}))

 * Named Group 
   date_namedGroup = re.match(r'(?P<year>[0-9]{4})-(?P<month>[0-9]{2})-(?P<day>[0-9]{2})',date)
   print(date_namedGroup[0][0]) → 1956 ...

## CAPTURING GROUP    (?P<group>regexp)
regexp = re.match(r'(?P<year>\d+)-(?P<month>\d+)-(?P<day>\d+)',date)
regexp.group('year') → 1956

## GRAB HASH SERVER FROM STR BELOW 
 REGEXP: server_matches = re.search(r'server:\s"(?P<server>[^"]*)',str(video_params))

window.globParams = {
  video: {
    can_dl: 0,
    id: "-53425992_165397466",
    access_token: "dbBO64b4o9TQOAbYE0Ip3G8qJYdJgASqWUR",
    credentials: "0cwtF9Zn07p41TOlfs4XJmb8i2AVRSJHlIYabnPeRUNooBt7dBLUni7l_qT40iYIrKmMC-wEwH-PRfpStf-PM36yO1oQGoerJfS3MuDUYIZ_0_36_kKA8sUlpcScXmeeqigG6fgvsBQ3nFYk-06BSKVbGgL",
    extra_key: "QRAz3IJxKuPH9wAQ6jcFzg",
    partial: {"quality":{"240":"pTXcW0l0WdXO-ChINveVfA","360":"v-vCWY7bZaFdNDs4FCMhdQ","480":"Jp61mIvZ4vsorPTn_3tMuw","720":"d9TZtB7E4I15UCu2uefduQ"}}
  },
  video_ext: null,
  server: "==Qdy5CZ19GbjlnehJ3YukTO5kTO5kTL2QTM2NHc",
  client_ip: "31.37.211.84",
  c_key: "e2613699762ccc8d",
  i_key: "02d029e0",
  ip_h: "4a43fcd43b0c3afe025042f6b50cd111",
  referer: "https:\/\/daftsex.com\/watch\/-53425992_165397466",
  quality: 360,
  logging: 1
};


## NON CAPTURING GROUP (?:)
 WITHOUT:
    url='http://stackoverflow.com/'
    pattern='(https?|ftp)://([^/\r\n]+)(/[^\r\n]*)?'
    match= re.search(pattern,url)
    print(match.groups())  → ('http'://'stackoverflow.com','/')

  WITH:
    pattern='(?:https?|ftp)://([^/\r\n]+)(/[^\r\n]*)?'
    match= re.search(pattern,url)
    print(match.groups())  → ('stackoverflow.com','/')
    print(match.groups(1))  → 'stackoverflow.com'


## YEWTU.BE URL EXAMPLE
  url='https://yewtu.be/watch?v=ySQzXpYETXc'
  pattern='https://yewtu.be/watch\?v=(?P<id>\w+)'
  match = re.search(pattern,url)
  print(match.groups()) → ySQZXpYETXc


