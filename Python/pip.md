# ---- [ PYTHON Pip ] ----

    Instal pkg from PyPI
        pip install spotDL

    Install pkg fom local wheel file
        pip instal requests-2.22.0.whl

    Install from Git repository
        pip install git+https://github.com/psf/requests.git

    Install Specific version 
        pip install requests==2.22.0

    Install pkg from a requirements file
        pip install -r requirements.txt

    Search for pkg mentioning term
        pip search <sorm term>

    Show Details of pkg
        pip show <some pkg>

    List all modules installed by PIP
        pip list

    Capture all currently installed versions in a text file
        pip freeze > requirements.txt

    Upgrade pkg
        pip install --upgrade spotDL

    Remove pkg 
        pip uninstall spotDL
