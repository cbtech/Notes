# ---- [ GENERAL PYTHON PROGRAMMING ] ---- 

## INDEX
- [Func / Lambda](Func.md)
- [File.md](File)
- [Input-loops.md](user Input and loops)
- [Class.md](Class)
- [Condition.md](If then else)
- [Exceptions.md](Exceptions Python Programming)
- [String.md](Python string)
- [Json.md](JSON Python Programming)
- [Advanced.md](Matrix /__init__/ __add__/)

* Containers:
  - [Array.md](Array)
  - [Dict.md](Dictionnaries)
  - [List.md](Python list)
  - [Tupples.md](Python tupples)
  - [Thread.md](Thread Python)
  - [Map.md]( Transform all the items in an iterable without loop)

* EXAMPLES
  - [../../SNIPS/PYTHON/dunders.py] (dunders)
  - [../../SNIPS/PYTHON/scraping_urllib3.py] (Web scraping,urllib3)


 [Panda Programming](panda.md)
 [Numpy Programming](numpy.md)

## SITE-PACKAGE
  python -m site  →   List all include directory

## LIST COMPREHENSION
    List comprehension is an elegant way to define and create a list in python.
    We can create lists just like mathematical statements and in one line only. 
    The syntax of list comprehension is easier to grasp.

## FUNCTION PARAMETER WITH HINT TYPE
    def myfunc(parameter:parameter_type) -> return_type:
    Example:
        def greet(age:int) -> str:
            return f"Hello! You are {age} years old ."
            
            ''' age : int indicates that the 'age' should be an int
            -> str: indicates that the function will return a string

## SHEBANG 
    #!/usr/bin/env python3

    from invidious_viewer import invidious_viewer
    invidious_viewer.main()

## BYTES
  str = '...' literals = a sequence of Unicode characters (Latin-1, UCS-2 or UCS-4
  bytes = b'...' literals = a sequence of octets (integers between 0 and 255)

## CONVERT HEX COLOR TO DECIMAL
    FFC200 → 255,194,0
    print (int('FF',16)) → 255
    print (int('C2',16)) → 194
    print (int('00',16)) → 0

## MODULE DIRECTORY 
```python
    # randomNumber is located to Resources/randomNumber
    from Resources.randomNumber import Calc
    # If the file is located ../Test/
    ..Test/

