# ---- [ PYTHON  - MAP ] ----


* Basic Example
 def square(number):
      return number ** 2


 numbers = [1, 2, 3, 4, 5]
 squared = map(square, numbers)

 list(squared)  Output → [1, 4, 9, 16, 25]

* Example using lambda
    numbers = [1, 2, 3, 4, 5]

    squared = map(lambda num: num ** 2, numbers)

    list(squared)       Output → [1, 4, 9, 16, 25]
