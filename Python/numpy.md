# [ Numpy Programming ]

  - [Numpy examples](../../SNIPS/PYTHON/numpy_exmaple.py)

## np where
    returns elements choosen from x or ydepending on condition.
    
## Arrays porperties and operations
```py 
   len(array)                   Return length of axis(0)
   array.shape                  Return a tupple with lengthof each axis
   array.ndim                   Number of dimmensions(axe)
   array.sort(axis=1)           Sort array along axis
   array.flatten()              Collapse array into 1 dimension
   a.astype(np.int16)           Cast to integer
   a.tolist()                   Convert array to list
   np.argmax(array,axis=1)      Return index of maximum along a given axis
   np.cumsum(array)             Return cumulative sum
```
## Array initialisation

## Rounding

## Elementwise operation and Math function

## Indexing

## Linear Algebra

## Reading/Writing files

