# --------------- [ ARRAY PYTHON ] -------------------

## Create array
    cars = ["Ford", "Volvo" "BMW"]
 * Inititalizes all the 10 spaces with 0
    a = [0] * 10

## Get the value of the first item
    x = cars[0]

## Get the length of array
    x = len(cars)

## Looping throught array
    for x in cars: 
        print(x)

## Adding Array Elements
    cars.append("Honda")

## Removing Array Elements
    delete the second elent from the array
        cars.pop(1)
    or
        cars.remove("Volvo")

## Method 	                Description
append()	        Adds an element at the end of the list
clear()	            Removes all the elements from the list
copy()   	        Returns a copy of the list
count()	            Returns the number of elements with
                        the specified value
extend()	        Add the elements of a list (or any 
                        iterable), to the end of the
                        current list
index()	            Returns the index of the first element 
                        with the specified value
insert()	        Adds an element at the specified position
pop()	            Removes the element at the specified 
                        position
remove()	        Removes the first item with the 
                        specified value
reverse()	        Reverses the order of the list
sort()	            Sorts the list

