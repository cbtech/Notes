# ---- [ BASH CONFIGURATION ] ----

## CONFIGURATION

* LOAD BASHRC AUTOMATICALLY
	edit .bash_profile
```sh
	if [ -r ~/.bashrc ]; then
		source ~/.bashrc 
	fi
```

## RUN COMMAND ON SPECIFIC TTY
  Add this to ~/.profile
    if [[ "$(tty)" == "/dev/tty1" ]]
	    then
	    	yaft
    fi

## COLORS
Black       \e[0;30m     Dark Gray     \e[1;30m
Blue        \e[0;34m     Light Blue    \e[1;34m
Green       \e[0;32m     Light Green   \e[1;32m
Cyan        \e[0;36m     Light Cyan    \e[1;36m
Red         \e[0;31m     Light Red     \e[1;31m
Purple      \e[0;35m     Light Purple  \e[1;35m
Brown       \e[0;33m     Yellow        \e[1;33m
Light Gray  \e[0;37m     White         \e[1;37m

## STYLE
    * Bold 
        replace 0 by 1 \e[1;30m for Black
    * Underline
    	replace 0 by 4 \e[4;30m for Black
    * Background
    	replace \e[40m for Black
    
    * Reset
	\e[0m

	echo "\e[0;30mHello"
	printf "\e[0;30m%s" Hello"

## ESCAPE SEQUENCE
    echo -e "\033[0;36mcbtech"

## MOVEMENT	
* Position the Cursor(Cursor at line L and column C): 											\033[<L>;<C>H or \033[<L>;<C>f 

* Move the cursor up N lines:		\033[<N>A
• Move the cursor down N lines:		\033[<N>B
• Move the cursor forward N columns:	\033[<N>C
• Move the cursor backward N columns:	\033[<N>D
 Clear the screen, move to (0,0):	\033[2J
• Erase to end of line:			\033[K
• Save cursor position:			\033[s
• Restore cursor position:		\033[u

# KEYS
 * To find sequence run: sed -n l
   and enter key
	^M Enter
	^[ Esc

ESC Code Sequence    Description
\033[H                moves cursor to home position (0, 0)
\033[{line};{column}H moves cursor to line #, column #
\033[{line};{column}f
\033[#A               moves cursor up # lines
\033[#B               moves cursor down # lines
\033[#C               moves cursor right # columns
\033[#D               moves cursor left # columns
\033[#E               moves cursor to beginning of next line, # lines down
\033[#F               moves cursor to beginning of previous line, # lines up
\033[#G               moves cursor to column #
\033[6n               request cursor position (reports as 033[#;#R)
\0337                 save cursor position (DEC)
\0338                 restores the cursor to the last saved position (DEC)
\033[s                save cursor position (SCO)
\033[u                restores the cursor to the last saved position (SCO)
	
