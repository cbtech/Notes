# [BLENDER]

## 3D View Navigation
Middle Mouse Button (MMB): Rotate view  
Shift + MMB: Pan  
Mouse Wheel: Zoom in/out  
Numpad 0: Camera view  
Numpad 1: Front view  
Numpad 3: Side view  
Numpad 7: Top view  
Numpad 5: Toggle perspective/orthographic  
Numpad 9: Opposite view  
. (Numpad dot): Center view on selected object  

## Object Manipulation  
G: Move (Grab)  
R: Rotate  
S: Scale  
G + X/Y/Z: Move along an axis  
R + X/Y/Z: Rotate along an axis  
S + X/Y/Z: Scale along an axis  
Shift + D: Duplicate  
Alt + D: Duplicate (linked instance)  
X / Delete: Delete object  
M: Move to collection  
Ctrl + A: Apply transformations  
Ctrl + J: Join selected objects  
Tab: Toggle between Object and Edit mode  
Edit Mode (Modeling)  
E: Extrude  
I: Inset  
Ctrl + R: Add edge loop  
K: Knife tool  
F: Create face  
Alt + M: Merge vertices  
P: Separate object  
L: Select linked element  
Alt + Click: Select edge loop  

## View and Camera Management  
Shift + A: Add new object  
Ctrl + Alt + Numpad 0: Align camera to current view  
G + MMB (in camera mode): Move camera  
R + R: Free rotation (Trackball)  
F12: Render image  
Ctrl + F12: Render animation  

## Miscellaneous Shortcuts  
Ctrl + Z: Undo  
Ctrl + Shift + Z: Redo  
Ctrl + S: Save  
Ctrl + N: New file  
Ctrl + O: Open file  
F3: Command search bar  
Spacebar: Play animation  
