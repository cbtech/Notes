# ---- [ TRYDACTIL ] ----

:tutor Run tutorial about tridactyl

f | ;h 	    Hint mode | Full hint mode
v		    Select mode
b 		    (buffer) select tabs
w o t 	    Open URL in new window / current tab / new tab
s		    Search in current searh engine
yy		    Copy the current URL to the clipboard
zi | zo 	Zoom in | Zoom out
gg		    Top of the page / g Bottom 
 

