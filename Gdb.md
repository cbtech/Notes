# ---- [GDB HOW TO] ----

		<[index.md]

    [Valgrind.md]( Valgrind explanations)

[READ]
https://www.cse.unsw.edu.au/~learn/debugging/modules/gdb_call_stack/

## OUTPUT TO NEW CONSOLE
    tty → /dev/pts/4
    <gdb>tty /dev/pts/4
    <gdb>run

## DEBUG IN LINKED LIST
    set var node=0x4051b0 ← you can change this adresse to see *node content
    p *node 

## DEBUG FWRITE
    After execute fwrite command, you can check the content with
        cat file

## STEP OVER FOR LOOP(until command)
   300   for(int ...) {
   301          ....
   302          .... }
   303  other instruction

   > until 303  → avoid for loop and continue after

  * Exit the current function and return to the calling function
        <gdb> finish

  * Step for assembly instruction:
        <gdb> stepi

## LAYOUTS
    * View src and asm(same time)
        CTRL+x 2

    * Asm layout + src
        layout split
    
    * Register layout
        layout reg

    * Repeat last print command 
        focus(fs) cmd       → insert mode

## REMOTE DEBUGING
 	gdbserver :9999 ./FBPAD

	* Create a client and join the server 
	gdb --tui 
	 <gdb>target remote :9999
	 b fb_init	→ Put a BP on function
	 c		→ Continue to execute program.

## DEBUG A CHILD PROCESS 
	 set follow-fork-mode <mode>
	 	mode are parent/child

## DEBUG A RUNNING APPLICATION
	5001 is pid of running fbpad
	cgdb ./fbpad -p 5001

## ATTACH TO PROCESS
    ps ax | grep ydotoold	→ Returns 567 as PID
	
    1. Run App and after GDB 
    2. (inside GDB) attach 567(PID)
    3. breack fbpad.c:150
    4. continue

## BREAKPOINTS 
    * Set bp on conditions
         break main.c:10 if i==5
    
    * Disable/Enable break 2 
        disable breakpoints 2
        enable  breakpoints 2
    * Save/Load breapoints for a further session
        save breakpoints bp.txt
        source bp.txt

## FRAMES 
 > bt full
#0  show_color_info (
    color=0x555555558240, len=16,
    colorname=0x5555555561c0 "RED")
    at minimal_fbcolormap.c:63
#1  0x0000555555555620 in main (argc=1,
    argv=0x7fffffffe8f8)
    at minimal_fbcolormap.c:113

 > frame 0 → show information about frame 0
   p color → Sow informations about var inside frame

## CHECKPOINTS
   > checkpoints
   > info checkpoints 
        * 0 process 23834 (main process) at 0x0
        1 process 23860 at 0x55555555574e, file minimal_fbcolormap.c, line 140

   > restart 1  → Restart at ceratin moment in the program
                  useful when you want to restart the program
                  at the begining
   
   > delect checkpoint 1

## POSTMORTEM PROCESS(core dump)
    ulimit -c unlimited 
    cat /proc/sys/kernel/core_pattern
    
    * Create new core file(under SU)
        echo "./new_core" > /proc/sys/kernel/core_pattern

    * Run gdb with core dumped
        gdb ./myapp ./new_core

(Inside GDB)
    > gcore      → Generate core.18991 file
    > target core core.18991

## SIZE OF DYNAMIC ARRAY
   struct buffer_t *buffer = malloc(struct buffer_t);
   buffer->content = malloc(256);

## ANALYSE CHAR *
	char *src = "Hello, My name is JackLemaitre. and I live Toronto.";

	p src → $1 = 0x555555556008(adresse of the firt byte) "Hello...."
	x/19bc src
	   0x555555556008: 72 'H'  101 'e' 108 'l' 108 'l' 111 'o' 44 ','  32 ' '  77 'M'
	   0x555555556010: 121 'y' 32 ' '  110 'n' 97 'a'  109 'm' 101 'e' 32 ' '  105 'i'
	   0x555555556018: 115 's' 32 ' '  74 'J'

	p *src@60 ← 60 is the size of 

	p *argv@argc is very useful to see information about argument

## PRINT VAR TYPE INFORMATION
    ptype var → ptype buffer->content
    whatis var → whatis 
    explore buffer

 * Print informations about array
    p sizeof(arr) /sizeof(*array)
 
 * Set a variable
    <gdb>set var i = 40

## INFO(How to see memory mappings)
    info proc mappings
	
    bt full → show all app informations 


## USE SANITIZER
	Install libsanitizer-devel
  * Build with gcc
  	gcc -g -Wall -o1 -std=c11 -fsanitize=address -fno-omit-frame-pointer daftscrape.c -l curl -o 

## RUN WITH COMMANDLINE ARGS
  cgdb wofi
  r --show run

 - ptrace operation not permited
     Solve this temporary, do the command below:
      echo 0 > /proc/sys/kernel/yama/ptrace_scope

     Solve permanenly, edit /etc/sysctl.d/10-ptrace.conf
      kernel.yama.ptrace_scope=0

      or simply run cgdb as root user
    
## DISPLAY MULTIPLE VAR IN ONE LINE
	define my_vars
	 disp i
  	 disp position
	 disp value
	 disp exp
	 disp s[i]
	end

 * After Compilation, enter my_vars to show vars
 
 * display all local vars: info locals
## SFML DEBUG
			
* 	Put a BP on sf::Keyboard::isKeyPressed(sf::Keyboard::Up)
			if sf::Keyboard::isKeyPressed(sf::Keyboard:Up)
				
* 	TODO:
		Didn't find mov DWORD PTR [esp] to show hello world ... P35
		Try to repere in ASCII CODE  Hello word 
		
	[Asm.md](Assembler code SRC)

## TOOLS COMMAND 
	objdump -D a.out | grep -A20 main.
* LIST ALL STRINGS IN LIB/EXECUTABLES
	strings ./executable

## GDB COMMANDS
* Set intel instruction. Put set disassembly intel in .gdbinit
    disassemble main	         List ASM instruction	
    list main   	          Show main function
    x			          Display the memory contents
    info locals			  Print local automatics only
    info register   	          Show all Register informations
    whatis expression		  Print type of expressions
    info register edi(i r edi)    Show informations about edi

* SHOW INFORMATION ABOUT
   x/bd 0x40200b	Show value stored at this memory address
     read a byte(b) and display the result as decimal(d)
     READ AS HEXADECIMAL → x/bx
     READ 2 BYTES WITH h
     READ 8 BYTES WITH g
     
   xo value       Octal information
   xx             Hexadecimal information
   xd → 19d    Decimal information(print 19 decimal)
   xu	        	Unsigned Decimal info
   xt	        	Binary information
   xf             Floating point info
   xa             Address
   xc			    c for char	
   xs			    s for string
   xi $rip   		Show instruction pointed by eip
   x3i 	   	    Show 3 instructions

* Unity Size	
   /b	   Octet
   /h	   Half Word (2 octets 16bits)
   /w	   Four Words (4 octets 32bits)
   /g	   DWORD(8 octets 64bits)
    
*  	Tips:
 * SHOW BINARY VALUE(346)
   echo $((0x15a))
   printf '%d\n' 0x15a	
   
*  TUI
  layout asm	Show ASM code
  layout src	Show SRC code

## ASM INSTRUCTIONS 
	$RIP					contains the value in RIP

* 	For loop in x86
			mov DWORD PTR[ebp-4], 0x0							Copy 0 in ebp-4
			cmp DWORD PTR [epb-4], 0x9							Compare ebp-4 to 9
        jle 0x00.......											Jump if less or equal
        jmp														else jump to this addr

## CHECK POINTER WITH GDB
	char *pointer = str_a

*	 View pointer Information
 x/xw pointer		0x7fffffffe5f0: 0x6c6c6548 string is located at 0x7fffffffe5f0
 x/xw &pointer 		0x7fffffffe5e0: 0xffffe5f0 
 x/s	pointer		Show string `Hello World`
 print &pointer		$1 = (char **) 0x7fffffffe5e0
 print *pointer

# ---- [ HOW TO DEBUG PROCESS ] ---- 

## WHEN AN APPLICATION FAILS
	   Run it from the command line.
	   The folowing command find the name of executable in package
 
 		for f in `pacman -Ql packagename | grep "/bin/" | cut -d" " -f2`; 

 		 		do file $f 2>/dev/null | grep -q executable && basename $f; 
 		done
		
## SEGMENTATION FAULTS 
	gdb appname
		`r` (wait for the segfault)
		`bt full` 
		Now pots the output to a Pastebin client and include the URL in your bug report
		Recompile the application in question with <t0 -g -O0 -fbuiltin> flags.Make sure "!strip" is in the options array 
		in the PKGBUILD, then install the package and run it again with gdb.
		This is what the options line can look like: 
						`	options=('!strip')`
		
Put these two lines at the very begining of the build() function of PKGBUILD:
		`export CFLAGS="$CFLAGS -O0 -fbuiltin -g"`
		`export CXXFLAGS="$CXXFLAGS -O0 -fbuiltin -g"`
						
Use core file with GDB:
		`gdb appname core`
		`bt full`
					
## VALGRIND 														
   usage:	`valgrind appname`					
	      	`valgrind --tool=callgrind appname`								
						
## STRACE 																			
	* For finding which files a program named appname tries to open			
				`strace -eopen appname`													
						
	* If you wish to grep the output from strace:
				`strace -o /dev/stdout appname | grep string`

## READELF ##
    * If you get "no such file or directory when running app
				`readelf -a /usr/bin/appname | grep interp`
		
 
