# [INDEX] 

## [ TODO ]  
[Todo List](TODO.md)

## [ 3D ]
[Blender shortcuts](Blender.md)

## [ CRYPTO ]  
[Relative Strength Index](Crypto/RSI.md)  
[Backtesting using backtrader](Crypto/Backtrader.md)
[TradingSystemusing Pandas and Numpy](Crypto/Backtesting.md)
[Trading (Bitcoin/Ethereum)](Crypto/Trading.md)  
[Fiscality (Taxation)](Crypto/Fiscalité.md)  

## [ LEXIQUE ]  
[File descriptor/Wayland/EGL/DRM, HTTP/Cookies/kworker](lexique.md)  

## [ELEC]
[Raspberry Pi](Rpi.md)  
[Arduino-cli](Arduino.md)  

## [CHESS] 
[Openings for White](Chess/whiteOpenings.md)  
[Openings for Black](Chess/blackOpenings.md)  

## [ANDROID]
[Android-studio](Android/Android.md)  
[Gradle BuildTool](Android/Gradle.md)  
[SDK Tools](Android/Sdkmanager.md)  

## [ PYTHON ]  
[Python Programming](Python/Python.md)  
[Panda Programming](Python/panda.md)  
[PIP Cheatsheet](Python/pip.md)  
[Pygame Programming](Python/Pygame.md)  
[URWID TUI Programming](Python/URWID.md)  
[Qtile Programming](Python/Qtile.md)  
[I3 Python Scripting](Python/pythonI3.md)  
[Regexp](Python/regexp.md)  

## [ Vi IMprovement ]  
[Vi Improvement](Vim.md)  
[Color/Syntax](Vim_colors.md)  

## [ ASSEMBLY ]  
[Assembly x86](asm.md)  

## [ C-LANGUAGE ]  
[C Programming](C.md)  
[Framebuffer/stty](C11/Framebuffer.md)  
[Linked List Programming](C11/Linked_list.md)  
[Ctags](Ctags.md)  
[Regular Expression](C11/regexp_c.md)  

### BOOKS   
[The Linux Programming Interface](LPI.md)  

## [ PSEUDOCODE ]  
[Pseudocode](Pseudocode.md)  

## [ CPP ]  
[C++ Programming](C++/C++.md)  
[CMake Guide](cMake.md)  
[TinyXML2](TinyXML2.md)  
[FLTK](FLTK.md)  
[Create a Tiling WM using XLIB](WM.md)  

## [ UNIX ]  
[UNIX FreeBSD](Freebsd.md)  

## [ LINUX ]  
[Pkgconfig/Meson/Ninja/Autoconf/Building/JAVA](Pkg.md)  
[General Commands](General.md)  
[Void Linux](LINUX/VoidLinux.md)  
[Arch Linux](Linux/ArchLinux.md)  
[Alpine Linux](Linux/Alpine.md)  
[Debian/Kali](Linux/Debian.md)  
[Linux From Scratch](Linux/LFS.md)  
[OpenSUSE](Linux/opensuse.md)  
[CUPS/Printers](CUPS.md)  
[Suckless (Dwm, ST, Tabbed, etc.)](Suckless.md)  
[Xorg/Autologin](Xorg.md)  
[Sway/Weston](WM.md)  
[Tmux](Tmux.md)  
[Wine/Gaming](Wine.md)  

###  IMAGE CREATION / KERNEL   
[Dracut Tools](Dracut.md)  
[Mkinitcpio Tool](Mkinitcpio.md)  
[Kernel Notes](Kernel.md)  
[CryptSetup/LUKS](Encryption.md)  

### SERVICES   
[OpenRC](Openrc.md)  
[Runit Service](Runit.md)  
[SystemV](SystemV.md)  
[SystemD](Systemctl.md)  
[Autologin/Getty/Agetty](Autologin.md)  

### BOOTLOADER   
[Syslinux](Syslinux.md)  
[UEFI](UEFI.md)  

### OTHER   
[GPG/AES256 Encryption](Encryption.md)  
[Firewall (pf/ipfw/iptables)](Firewall.md)  
[ALSA (Advanced Linux Sound Architecture)](Alsa.md)  
[Networking](Network.md)  
[Partitioning Tools](Partitionning.md)  
[Printer](Printer.md)  
[Wayland Development](Wayland.md)  
[Media Players (MPV, Kodi)](MPV.md)  
[Virtualization (QEMU/KVM/Docker)](VirtualM.md)  
[IRC](Irc.md)  
[Xdotool](Xdotool.md)  
[Cron Jobs](Cron.md)  
[Secure Shell (SSH)](Ssh.md)  
[OpenSSL](OpenSSL.md)  
[Plowshare Downloading Tool](Plowshare.md)  
[Ripgrep/FZF](ripgrepfzf.md)  
[Terminal Multiplexer (Tmux)](Tmux.md)  
[Customization (FrameBuffer, Themes, etc.)](Customization.md)  
[File Compression](FileCompression.md)  
[Git](Git.md)  
[Awesome WM](Awesome.md)  
[Jack Audio](Jack.md)  
[Virtual Machines](vm.md)  


## [ TECH ]  
[Serial/OBD/CAN Bus](OBD2.md)  

## [ JAVA ]  
[Java Programming](Java/Java.md)  
[Kotlin Programming](Kotlin/Kotlin.md)  

## [ GO ]  
[Go Programming](Go.md)  

## [ VIDEO / AUDIO EDITING ]  
[FFMPEG](FFMPEG.md)  

## [ BASH ]  
[Scripting/Useful Commands](BASH/Bash.md)  
[Configuration/CSI/Colors/Underline/Bold](BASH/Bash_config.md)  
[Bash Timer Programming](BASH/Timer.md)  
[Keybindings/Expansion/Substitution](BASH/Keybindings.md)  
[I3 Scripting](BASH/bashI3.md)  
[Curl/Invidious](Curl.md)  
[Coreutils Tool (cut/tr/sed)](Coreutils.md)  

## [ KEYBINDINGS ]  
[Manage Keybindings](Keybindings.md)  
[Firefox/Trydactyl](Tridactyl.md)  
[Youtube downloader Keybindings](yt_dlp.md)

## [ WWW ]  
[Minimalistic WebBrowser](Webbrowser.md)  
[EcmaScript](Javascript/Javascript.md)  
[Cascading Stylesheet](CSS.md)  
[HTML](HTML.md)  

## [ MYSQL ]  
[MySQL](Mysql.md)  

## [ LUA ]  
[Lua Scripting](Lua.md)  

## [HACK]  
[GDB Debugging](Gdb.md)  
[Web Developer Tools](ETHICALHACK/Wdt.md)  
[Streaming Site Hacking](ETHICALHACK/Streaming.md)  
[Hack The Box Guide](ETHICALHACK/HackTheBox.md)  
[Ethical Hacking (Ettercap/Nmap)](ETHICALHACK/HackCMD.md)  
[Hacking Terminology](ETHICALHACK/HackExplained.md)  
[iPhone/iPad Jailbreaking](ETHICALHACK/Phonehack.md)  
[Samsung Galaxy Hacking](ETHICALHACK/Samsunghack.md)  
[Apple Vulnerabilities](ETHICALHACK/Apple_VULN.md)  

## [EDUCATION] 
[Obtenir_sans_punir - Christophe Carré](Education/Osp.md)  
[Motivation_Chalenges](Education/Chalenges.md)  

## [ HARDWARE ]  
[Zalman Z11](ZalmanZ11.md)  

## [ WINDOWS ]  
[Windows 10](Windows.md)  

## [ APPLE ]  
[Apple (iFuse, iBoot)](Apple.md)  

## [ HOW IT WORKS ]  
[TCP/IP](Tcp.md)  


