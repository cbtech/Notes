# ---- [ OBD PROGRAMMING ] ---

	[index.md] (Go Back)
## RES
    https://en.wikipedia.org/wiki/ELM327
    https://github.com/iDoka/awesome-canbus
    https://docs.devsdev.com/obd2/obdii-simulators/github-elm327-emulator-for-testing-software-interfacing-obdii-via-elm327-adapter-supporting-multi-e
    https://en.wikipedia.org/wiki/OBD-II_PIDs#Service_01
    https://yewtu.be/channel/UCZDA1kA3y3EIg25BpcHSpwQ/playlists
    https://www.csselectronics.com/pages/obd2-pid-table-on-board-diagnostics-j1979
    https://github.com/brendan-w/python-OBD/blob/master/obd/elm327.py

## CONNECT USING ECHO
    * SET INTERFACE
      stty -F /dev/ttyUSB0 speed 115200 cs8 
      		-cstop  -parenb -echo
      cat -v /dev/ttyUSB0

   Open a new terminal 
   	echo -e "AT Z\r" > /dev/ttyUSB0

## FILES
  [../WORK/PYTHON/OBD/read_DTC.py](Read DTC using Serial)

 * When connecting interface dmesg shows:
     FTDI USB Serial ... attached to ttyUSB0
     interfaces are listed in /dev/serial/by-id/<interface_name>
    
# AT Command 
    At command 
# ELM323/327 OBD2 Playground

The EML chipsets can be controlled using `AT` commands. These set configuration options on the OBD2 connector, such as which protocol to use communicating with the vehicle.
I will be using a generic bluetooth module with an ELM327 v2.1 chipset, and an ELM323 v2.0 USB cable.

## Initiating the ELM323/327

Connect to the OBD2 adapter from a serial terminal app and start issuing the following commands.
    > AT Z

This command resets the ELM327 interface so we are starting from a known good point. Once reset the interface should respond with `ELM327 v2.1`. 

ELM327 Only: If this is the first time you have connected the interface to the vehicle you may want to set the protocol (for the Passat this is ISO-14230-4 KWP):
    > AT TP 5

## Checking the battery voltage (ELM327 only)

Before we start issuing commands to the ECU of the vehicle we can check that we are talking to the ELM327 correctly by asking it check the voltage of the vehicle battery by sending the following command:
    > AT RV

This will respond with a value such as `12.6V`. When the engine is not running a value between 12v and 13v is normal, whereas when the engine is running this is typically above 13v and maybe as high as 15v. Because of the voltage difference between a running and not running engine, we can use this check in any code we write to see if the engine is running or not.

## Checking for OBD2 connectivity

To check if we can communicate with the vehicle we will send an OBD2 command to get the engine RPM.
    > 01 0C

If the command is successful it should return a value similar to:
    > 41 0C 1A F8

The first two bytes `41 0C` represents this is a response to our `01 0C` command, and the following 2 bytes are the RPM value `1A F8` which is 6904 in decimal. OBD2 Engine RPM values are sent as 1/4rpm intervals, so we divide 6904 by 4 to get 1736.

### Vehicle Speed

Request:
    > 01 0D

Response:
    > 41 0D 37

Vehicle speeds are returned in hexadecimal format and represent the speed in km/h. So in this case `0x37` represents 55km/h.

### Selected Gear
There is no way to get the currently selected gear directly from the ODB2 interface, however by using the ratio of engine RPM to vehicle speed we can determine which gear we are in.

Example: I know that in my Volkswagen Golf at 110km/h (68mph) I will be doing 2000rpm in 6th gear. So if we take 2000 divided by 110 we get a ratio of 18.181818182. So now if we read the engine rpm and engine speed over OBD2 and the ratio is around 18, we can infer that we are in 6th gear.

## Optimised Settings

The ELM323 chipset has a set baud rate of 9600, so it could be possible to max out the connection if we are making a lot of requests. The ELM323 supports a number of settings that can tweak the efficiency that data is passed over the line.

### Echo Off

By default any commands sent to the ELM chipsets repeated back to the host computer. To save bandwidth, we can turn this off by issuing the command:
    > AT E0

### Packed Data (ELM323 only)
