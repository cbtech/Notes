# --- [ SYSTEMV ] ----

 Runlevel 0: Halt
 Runlevel 1: Single user mode
 Runlevel 6: Reboot

 All System V init scripts are stored in /etc/rc.d/init.d
 or /etc/init.d

 
