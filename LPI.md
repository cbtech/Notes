# ---- [ Linux Programming interface Michael Kerrisk ] ----

# Chap 2: 
    Kernel : it is possible to run programs on computer without kernel. The
    presence of kernel simplifies the writing and uses of other program. 
    and increase the power and flexibility available to programmer.
    
    * Task performed by the kernel
    process scheduling:
            a computer has one or more central processing units(CPUs), which
            execute the instructions of program.Linux is preemptive multitasking
            OS. Multitasking means that multiple processes can simultaneously
            reside in memory. 
    
    Preemption:
            A preemptive kernel is one that can be interrupted in the middle of
            executing code. The main advantage of preemptive kernel is that
            syscalls do not block the entire system.If a system call takes long
            time to finish, the kernel can do anything at the same time.
            The main disadvantge is that this introduces more complexity to 
            the kernel code.

    A system call allows processes to request services from the kernel.

    Certain operations can be performed only while the processor is operating in
    Kernel mode(halt instruction,Accessing the memory management hardware,
    initiating device I/O).

    In many everyday programming tasks, We are accustomed to thinking about
    programming in a process-oriented way.

    The kernel maintains data structures that map the virtual memory of each
    process into physical memory of the computer and the swap area on disk.

## The Shell
    A shell is program designed to read the command typed by user and execute
    appropriate programs in response to those command.

## User:
    Superuser(root) has ID 0

## Directory:
    A directory is a special file whose contents take the form of a table of
    filename coupled with references to the corresponding files. This filename +
    reference is called a link.

    Absolute pathname → /home/mtk/.bashrc
    Relative pathname → ../mtk/.bashrc

## FileDescriptor
 Normally, a process inherits three open file descriptors when it is started by
 the shell. 
        - descriptor 0: standard input
        - descriptor 1: standard output
        - descriptor 2: standard error

## Process Creation and program execution
    The child inherits copies of parent's data,stack,and heap segments.

    Each process has a unique Process identifier(PID). Each process also has a
    Parent Process Identifier(PPID).A process return a termination status, a small
    non-negative integer value that is available for inspection by the parent
    process using the wait() system call.

## Capabilities
    Granting a subset of capabilities to a process allows it to perform some of
    the operations normally permitted to the superuser, while preventing it from
    performing others.

## The init process
    When booting the system linux create a special process called init. The init
    process as always the process ID 1 and runs with superuser privileges. The
    main task of init is to create and monitor a range of processes required by 
    a running system.

## Daemon
    * It is long lived. A daemon process is often started at system boot and
    remains in existence until the system shutdown.
    
    * It runs in the background, and has no controlling terminal from which it
      can read input or to which it can write output.

## Environment list
    Each process has an environment list, which is a set of environment variables
    that are maintened within the userspace memory of the process.

## Ressources limit 
    Each processs consumes ressources, such as open files, memory and CPU time.
    Each resources limit has two associated value: a soft limit, which limits the
    amounts of the resource that the process may consume and a hardlimit, which
    is ceiling on the value to which the soft limit may be adjusted.

## Memory mapping
    Mappings fall into 2 categories:
        A file mapping maps a region of a file into the calling process's virtual
        memory. Once mapped , the file's contents can be accessed by operations
        on the bytes in the corresponding memory region.

        By contrast, an anonymous mapping, doesn't have a corresponding
        file.Instead the page of the mapping are initiliazed to 0.

## Static and shared libraries
    Static librairies: The fact that each statically library creates a number of
    disadvantages. One is that the duplication of object code in different
    executable files wastes disk space.A corresponding waste of memory occurs
    when statically linked programs 


    Shared librairies: shared libraries were designed to adress the problems with
    static librairies.In shared librairies, the linkers create just a record into
    the executable to indicate that at runtime the executable needs to use that
    shared library.

## IPC Communication and synchronization.
    In linux, some processes, cooperate to achieve their intended purpose, and
    these processes needs methods of communicating with one another and
    synchronizing their actions.

    One way for processes to communicate is by reading and writing information in
    disk files.However, for many applications, this is too slow and inflexible.
    Therefore, Linux, Like all modern UNIX implementations, provides a rich set
    of mechanisms for IPC:

        1. Signals, which are used to indicate that an event has occured
        2. Pipes (familiar to shell users as |) and FIFO.(transfer data between
           processes).
        3. Sockets, which can be used to transfer data from one process to
           another
        4. File locking, which allows a process to lock regions of a file in
           order to prevent other processes from reading or updating the file
           content.
        5. Message queues, which are used to exchange message(packets of data)
           between process.
        6. Semaphore, synchronize the action of processes.
        7. Shared memory, Allows two or more process to share a piece of memory.

## Signals
    Signals are descibed as software interrupts.
    A signal handler is a function that is automatically called when the signal
    is delivered to the process.

## Threads
   In UNIX, each process can have multiple threads of execution.Each thread has
   it own stack containing local variables and function call linkage information.
   Thread can communicate with each other via the global variables.The primary
   advantages of using thread are that they make it easy to share data(via global
   variables).

## Session,Controlling Terminals, Controlling Processes
    A session is a collection of process group(jobs).

## Pseudoterminals
    A pseudoterminal is a pair of connected virtual devices, known as the master
    and the slave.The device pair provide an IPC channel allowing data to be
    transferred in both directions between the two devices.
    
## Client-Server architecture
    A client server application is one that is broken into two component
    processes

        Client: which asks the server to carry out some service by sending it a
        request message.

        Server: Which examines the client request, performs approprite actions.
   
## /proc file system
    The /proc filesystem is a virtual file that provides an interface to kernel 
    data structure in a form that looks like files and directories on a file
    system.
    
# Chap 3: System Programming Concepts

# CHAP 4: File I/O
        This chapter focus on the system calls I/O on disk files.
        All system calls for performing I/O refer to open files using
        filedescriptor.
    
    read system call doesn't place a terminating null byte at the end of the string.
    Write: multiples possibles reasons for such a partial write are that the disk was filled
           or that the process resources limit on file sizes was reached.

CHAP 6:  PROCESSES

    From the kernel point of view, a process consists o userspace memory containing program code and
    variables used by the that code.The information recorded in kernel data structures includes various
    identifiers numbers (ID) associated with the process,virtual memory tables ...
    
    Pages (Copies of the unused pages are stored in swap area). On x86_32 pages are 4096 Bytes in size.
    IA → 16386 Bytes Alpha → 8192 Bytes
