# ---- [ CSS ] ----

    [CSS/media_queries.md] (Different media queries)
    [CSS/layout.md] (Static/Absolute/Relative)

Comments →  /* */

## CSS IN HTML:
 * Internal Css
   <style>
      body {
       background-color: linen;
      }
   </style>

 * External css file:
     <link rel="stylesheet" href="mystyle.css">

## Modify multiples categories 

            .navbar li {
              display: inline;
              border-color: AliceBlue;
              &:before {
              color: Gray;
              content:" : "; 
              padding: 0 10px;
              }
              &:after {
              color: Gray;
              content:" : "; 
              padding: 0 10px;
              }

## DISPLAY 
        inline-block → Just inline text
        block           Multi line text


## MODIFY ID SELECTOR / CLASS SELECTOR
    class selector(.):
        is used when the same style is applied to multiple HTML elements on the same webpage
    id selector(#):
        is unique identifier of the HTML element

     Example:
        <textarea id="msg" rows="10" cols="100">Select a story to start.</textarea>
        id selector → #msg { } class selector → .msg

