# ---- [ VALGRIND ] ----

 valgrind --leak-check=full --track-origins=yes <PROGRAM>

 Invalid read of size 1 → no \0 at the end of the string.
    at 0x4848814: __strlen_sse2 (in /usr/libexec/valgrind/vgpreload_memcheck-amd64-linux.so)
    by 0x4962760: __vfprintf_internal (vfprintf-process-arg.c:397)
    by 0x4956F0A: printf (printf.c:33)
    by 0x10984F: main (daftscrape.c:182)
	
## ERRORS: (https://bytes.usc.edu/cs104/wiki/valgrind/)
    ### X bytes in Y blocks are definitely lost 
    8 bytes in 1 blocks are definitely lost in loss record 1 of 7

    This error points you to where that memory was allocated
        
        Example: 
            int **matrix = new int*[2];
            matrix[0] = new int[2];  ← Error, memory allocated
            delete [] matrix;

### Conditional jump or move depends on uninitialized values
    
    This error is caused if you forget to initialize variables before 
    using or accessing them. You can use the flag --track-origins=yes to
    see where the uninitialized value came from

        Example:
            int s;
            if (s == 66) { // error
            printf("Hello there, General Kenobi!");
            }

### Invalid read/write size X
    
    This error happens if you deleted memory and tried to access it in
    some way.Valgrind will tell you where the read/write occured

    int * matrix = new int[2];
    delete [] matrix;
    matrix[1] = 2 // Error

### Mismatched free/delete
        This is caused by incorrectly deleting a pointer to an array
        If you're deleting a single object(int *) you can deallocate memory with delete

        However, if you're deleting ann array object(int **), you need to use delete[]
        This also for higher level pointers(int ***, int****)
        
#### Heap and Leak Summary
        Heap summary:
            tells you haw many memory allocation occured(how many times new was called
            directly or indirectly), and how many bytes of memory were lost.
            
        Leak Summary:
            Definitely lost → your program is leaking memory and you need to fix it.
            Indirectly lost → Your program may have crashed and couldn't clean up your memory
            Suppressed      → You can safely ignore this area since this memory was not managed 
                              by your program.
            Possibly lost   → Your program is leaking memory unless you're doing odd things 
                              with pointers


