# ---- [ PYTHON - ADVANCED TECHNIQUE] ----

## EXAMPLE
    [../../SNIPS/PYTHON/dunders.py](Dunders Examples)
    [../../SNIPS/PYTHON/matrix.py](Matrix Examples)
## MATRIX( 2D Array)

 * Representation 
    [0][0]  [0][1]  [0][2]
0 →   11      2       4
1 →   4       5       6
2 →   10      8      -12

## Creation of Matrix 
matrix = [[11, 2, 4], 
    [4, 5, 6],
    [10, 8, 12]]


## MAGIC METHOD (DUNDERS: Double UNDER Score Method)
  __add__	for +
  __sub__ 	for -
  __truediv__	for /
  __floordiv__	for //
  __mod__	for %
  __pow__	for **
  __and__	for &
  __xor__	for ^
  __or__	for |


* EXAMPLE :
```python
	a = 200
	b = 6
	c = a.__floordiv__(b)

	print(c)	# Print 33
```
