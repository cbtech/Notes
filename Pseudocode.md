# ---- [ PSEUDOCODE TUTORIAL] -----


## RULES
    1. Be clear and consistent
    2. Focus on Logic, Not Syntax
    3. Use Proper Indentation
    4. Keep it concise

    - Write only one statement per line
    - Write what you mean, not how to program it
    - Give proper indentation to show hierarchy and make
      more readable.


## KEYWORD in CAPS
    Constants: CONST
    Function and Procedure names: FUNC CALCULATE_SUM(a, b)
    Global Var: GLOBAL COUNTER = 0
    Comments:   // INITIALIZE VARS
    Input/Output: INPUT, OUTPUT, DISPLAY, PRINT
    Decision-making: IF, THEN, ELSE, ENDIF
    Loops: WHILE, FOR, REPEAT, UNTIL, ENDWHILE, ENDFOR
    Process: SET,CALCULATE, INCREMENT, DECREMENT

## WHILE    
    WHILE <condition is true> DO
        <instruction to repeat>
    ENDWHILE
    
    DECLARE Password: STRING
    INPUT Password
    WHILE Password <> "Secret123" DO
        OUTPUT "Try again"
        INPUT Password
    ENDWHILE
    OUTPUT "Acces Granted"

## EXAMPLES
    1. SPLIT STRING INTO WORDS
        START
            SET start = 0
            SET end = 0
            SET words = an empty list
            SET word = an empty string

            WHILE i < length of the string
                IF character at end is not a space THEN
                    ADD character at end to word
                ELSE
                    IF word is not empty THEN
                        ADD word to words
                        SET word to an empty string
                    ENDIF
                ENDIF
                INCREMENT end
            ENDWHILE

            IF word is not empty THEN
                ADD word to words
            ENDIF

            DISPLAY words
        END
            
    2. Calculating the Sum of Two numbers
        START
            DECLARE n1, n2, sum
            SET n1 = user input
            SET n2 = user input
            CALCULATE sum = n1 + n2
            DISPLAY sum
        END
    3. FINDING THE MAXIMUN NUMBER IN A LIST
        START
            DECLARE list, max
            SET list = array of numbers
            SET max = first element in list
            FOR each number in list DO
                IF number > max THEN
                    SET max = number
                END IF
            END FOR
            DISPLAY max
        END
    4. Bubble sort Algorithm
      START
        INPUT list of numbers 
        SET n = length of the list
        REPEAT
            SET swapped = false 
            FOR i FROM 1 TO n-1
                IF list[i] > list[i+1] THEN
                SWAP list[i] and list[i+1]
                SET swapped = TRUE
                ENDIF
            END FOR
            DECREMENT n by 1
        UNTIL swapped == false 
        DISPLAY list
      END


    5. LESS OR MORE 
        START 
            CONST RANDOM_NUMBER = RANDOM()
            SET number = 0
            SET done = 0
            SET choice = 'y'

            WHILE done != 0
                INPUT number 
                IF RANDOM_NUMBER < number THEN
                    DISPLAY "Too small"
                ELSE IF
                    DISPLAY "Too Big"
                ELSE
                    DISPLAY "Well Done, Guy"
            DISPLAY "Do you want to play again[y/n] ?"
            IF choice == 'y'
                done = 0
            ELSE done = 1
            DISPLAY "Good Bye... See you soon"
        END





