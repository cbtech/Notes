# ---- [ YT_DLP] -----

## FORMAT

### AUDIO
140     m4a   audio only      2 |   12.66MiB   129k https | audio only    mp4a.40.2  129k 44k [fr] medium, m4a_dash

# VIDEOS
134     mp4   640x360     30    |   11.80MiB   121k https | avc1.4D401E   121k video only       360p, mp4_dash
135     mp4   854x480     30    |   17.65MiB   181k https | avc1.4D401F   181k video only       480p, mp4_dash
136     mp4   1280x720    30    |   26.37MiB   270k https | avc1.4D401F   270k video only       720p, mp4_dash
137     mp4   1920x1080   30    |   97.72MiB  1000k https | avc1.640028   1000k video only      1080p, mp4_dash
620     mp4   2560x1440   30    | ~676.11MiB  6917k m3u8  | vp09.00.50.08  6917k video only     1440p
625     mp4   3840x2160   30    | ~  1.67GiB 17494k m3u8  | vp09.00.50.08 17494k video only     2160p

To download the better quality don't specify -f format 

To download Separate audio and video 

136     mp4   1280x720    30    │   23.00MiB   393k https │ avc1.4d401f     393k video only       720p, mp4_dash
247     webm  1280x720    30    │   19.88MiB   340k https │ vp9             340k video only       720p, webm_dash
398     mp4   1280x720    60    │   22.07MiB   377k https │ av01.0.08M.08   377k video only       720p60, mp4_dash
311     mp4   1280x720    60    │ ~214.94MiB  3680k m3u8  │ avc1.4D4020     3680k video only
298     mp4   1280x720    60    │   39.78MiB   680k https │ avc1.4D4020     680k video only       720p60, mp4_dash
612     mp4   1280x720    60    │ ~136.77MiB  2341k m3u8  │ vp09.00.40.08   2341k video only


250     webm  audio only      2 │    3.95MiB    68k https │ audio only       68k 48k [en] low, webm_dash
140-drc m4a   audio only      2 │    7.57MiB   129k https │ audio only      129k 44k [en] medium, DRC, m4a_dash
251-drc webm  audio only      2 │    7.52MiB   129k https │ audio only      129k 48k [en] medium, DRC, webm_dash
140     m4a   audio only      2 │    7.57MiB   129k https │ audio only      129k 44k [en] medium, m4a_dash
251     webm  audio only      2 │    7.52MiB   129k https │ audio only      129k 48k [en] medium, webm_dash

    
    yt-dlp -f136+250 $(wl-paste)
    for 720p 136 | 609
 *  For 720p with audio python3 -m yt_dlp -f609+140 $(wl-paste)
