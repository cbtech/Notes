# ---- [ SYSTEMCTL ] ----

## Commands
```
 systemctl list-units --type=service --state=runninig
        or pstree	                                                List all services running

 systemctl enable/disable openvpn.service                           Enable / Disable services
 systemctl start/stop openvpn.service                               Start /Stop Service
 systemctl status polkit.service                                    Show status about units
 systemctl list-dependencies polkit.service                         List dependencies
 systemctl list-unit-files                                          List all Services
```
## Journal(Log)        
```
 journal --vacuum-size=100M             Clean journal files
 journalctl -b                          Show all msg from Current Boot
 journlactl -b1                         Show all msg from Previous Boot
 journal -u vlc                         Show all msg byt unit
 journalctl -p err..alert               Show only critical / Alert msg
```

## Services
	wpa_supplicant
	networking
	NetworkManager       

# -- [ PKGBUILD ] -- #
                
        ## PROTOTYPE ## 
                /usr/share/pacman/PKGBUILD.proto 
                
                epoch:		Used to force the package seend as newer than any previous version with a lower epoch.

                * Example:		1:5.13-2
                pkgver=5.13
                pkgrel=2
                epoch=1



