# [ ARCHLINUX ]

               < [index.md]
                
## PACMAN 
```
   pacman -S arch-wiki-docs                     Install offline Docs
   pacman -Ql <pkg>                             List files of installed pkg
   pacman -Qqe                                  List all installed packages
   pacman -Si <pkg>                             Show pkg informations
   pacman -Rns <pkg>                            Remove pkg with his dependencies
   pacman -U pkg.tar.xz                         Install Pkg from files
   pacman -Qm                                   List Manually installed
   pacman -Sw <pkg>                             Download pkg without install
   pacman -Scc                                  Clear cache
   pacman -Qo /usr/lib/libGLX.so                See file owned by pkg 
   dkms install nvidia/340.106 -k 4.4.46-lts     Install DKMS Module
```               

## Nouveau NVIDIA Driver
        ## INSTALL DRIVER AND DEPENDANCIES ##
                sudo pacman -S xf86-video-nouveau 
                               libglvnd linux-firmware mesa
                               mesa-vdpau
                               libvdpau-va-gl
                               vdpauinfo ( For informations)

## Change LoginManager
    yay -S ly
    systemctl disable sddm # lightdm or gdm etc
    systemctl enable ly.service

### Verify installation 
```
     glxgears                                       pkg: mesademos
     DRI_PRIME=1                                    Run application on the nvidia-gpu
     mpv --gpu-context=help                         Show different context
     mpv --gpu-api=help                             Show different API
     ffmpeg -hwaccels                               Show different hardware acceleration methods   
     glxgears 
```            
    https://wiki.archlinux.org/index.php/Hardware_video_acceleration

## KEY MANAGEMENT
* ARTIX EXTRA REPO
        pacman -S artix-archlinux-support
        pacman-key --populate archlinux

