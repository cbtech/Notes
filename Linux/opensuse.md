# ------------------ [ OPENSUSE ] -----------------

## INFORMATIONS
    Informations about pkg 
        rpm -qa | grep java
    List path of files installed
        rpm -ql <package_name>      → example 
                                      java-21-openjdk
    Install Emacs or remove vim     
        zypper install emacs -vim
    Cleanup dependencies 
        zypper rm --clean-deps <package_name>
    Verify dependenciers
        zypper verify
    Community package
        zypper ar -cfp 90 http://ftp.gwdg.de/pub/linux/misc/packman/suse/openSUSE_Tumbleweed/ packman
        zypper refresh
## ZYPPER
 * Manage Repos
        zypper repos
 * Remove Repos
        zypper removerepo 4                         → By number
        zypper removerepo "Non-OSS Repository"      → By Name

## KVM
    zypper install -t pattern kvm_server kvm_tools libvirt qemu virt-manager libvirt-daemon-driver-qemu qemu-kvm
    * Run and enable virtualization
        systemctl enable --now libvirtd

## KOTLIN 
    zypper addrepo --refresh https://download.opensuse.org/repositories/system:/snappy/openSUSE_Leap_15.5 snappy
    zypper --gpg-auto-import-keys refresh
    zypper dup --from snappy
    zypper install snapd

## FIREWALL
On Opensuse firewalld is installed and enablmed by default.It is a regular systemd service 
that can be configured via systemctl or YASR Services Manager.

   * Restart firewall
        firewall-cmd --reload
   or
        systemctl reload firewalld 

   ### LIST INTERFACES AND PORTS
        firewall-cmd --zone=public --list-interfaces
    
    * Ports
        firewall-cmd --get-services
    

## MPV Issues(video freeze)
  * Add packman Repository

    sudo zypper install --from packman gstreamer-plugins-bad gstreamer-plugins-ugly gstreamer-plugins-libav ffmpeg-7

