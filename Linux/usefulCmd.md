# Useful Command
 
## Umount busy filessystem
    `sudo lsof +f -- /dev/sdd1 or sudo fuser -m /media/usb`
    sudo kill -9 <PID>

    * show dmesg from previous session
        dmesg -T

    * Clear dmesg 
        dmesg -C

    * Run command from tty
   	    qutebrowser &> /dev/null &

   * CLEAN HARDRIVE 
   	wipefs -a /dev/sdx
	dd if=/dev/zero of=/dev/sdx bs=512 count=1 conv=notrunc

   * MOUSELESS IN Linux 
   	Shift + numlock+ 8462 → Up/Left/Right/Down
	setxkmpa -option  keypad:pointerkey

   * SHOW KEYCODE INFORMATION
     	showkey
	showkey --ascii    	→ show ascii keycode
	showkey --keycode 	→ show keycode
	showkey --scancode	→ show scancode(hex)

	kbdcontrol -t 		→ under freebsd

    * PROGRAMMING MAN PAGE
    	man 2 read

   * LIST ALL MANPAGES
    	apropos .
	man -k .

	- Limit to Section 3(Programming)
		man -k . -s 3(same with apropos)
	- Install Man programming pages
		man-pages-devel(Void linux).
	
    * SHOW SOCKET INFORMATION
       ss -l -p -x | grep /wayland-0

   * ADD GROUP / REMOVE
   	groupadd / groupdel _seatd	

   * CONNECTION ROOT IMPOSSBILE
   	-rwxr-xr-x root root 	/bin/su
	chmod u+s /bin/su to change permission to s

   * INFORMATIONS ABOUT FILES
	stat /usr/bin/X 

   * PASTE FILE TO PASTEBIN
    cat file | nc termbin.com 9999

   * Show Directory and size
      	dh -h
        du -h --max-depth=1		  Just Directory without file
        du -h -s                Just size of current directory 
   
   * Count line in All *.c *.h files
      `wc -l *.[hc]`

   * Copy multiple files(.vimrc,.vim to dest
       cp .{vimrc,vim} dest
       mv {ende.py,flag.txt.en,pw.txt} dest/

   * List file 
       `ls test*`      List all file named test
       `ls test?`      List test2/test3/test4/test5
       `ls test???`    List test340/test564/test700
       `ls -lh`        List Files with Size Kb/Mb/Gb
       `ls *.[hc]`     List all .H .C files 

   * Markdown files to PDF using pandoc and texlive 
      `pandoc file.md -o file.pdf`


