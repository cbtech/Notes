# --- [ Vim Color and CUstomization ] ----

        Red		LightRed	DarkRed
	    Green	LightGreen	DarkGreen	SeaGreen
	    Blue	LightBlue	DarkBlue	SlateBlue
	    Cyan	LightCyan	DarkCyan
	    Magenta	LightMagenta	DarkMagenta
	    Yellow	LightYellow	Brown		DarkYellow
	    Gray	LightGray	DarkGray
	    Black	White
	    Orange	Purple		Violet

# CUSTOMIZE COLORS(../DOTFILES/Z10/nvim/colors/custom-dark.vim)
    :hi to see all color syntax 
        Use tmux color script to show color
        ctermbg= bg with terminal color 
