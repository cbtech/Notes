# ------------- [ KOTLIN Programming] ---------------------

print is simillar to println but doesn't add new line

## COLLECTIONS
 A <read-only> interface provide operations for accessing collection elements
 A <mutable> interface that extends the corresponding read-only interface with
 write operations: adding,removing,and updating its elements.
 
### GENERICS
    Genrics  are a feature that allows classes, interfaces,methods, and properties
    to operate on types specified later
        [../../SNIPS/KOTLIN/generics.kt]
### ARRAYS
    [../../SNIPS/KOTLIN/arrays.kt](Array / Deque)

### LIST
     [../../SNIPS/KOTLIN/collection.kt](Collection / List)

 * Declare an empty list
        val fruits = listOf("banana", "avocado", "apple", "kiwifruit")
        myEmptyList: List<T> = emptyList()
        
### SET
   [../../SNIPS/KOTLIN/set.kt](Set)

### MAP(Dictionary)
   [../../SNIPS/KOTLIN/map.kt](Map)

## FUNCS
    [../../SNIPS/KOTLIN/functions.kt](Functions)  
## RANGE
    [../../SNIPS/KOTLIN/arrays.kt](Range)  
## IF/ELSE
    [../../SNIPS/KOTLIN/more_or_less.kt](More or less example)
## WHILE
    [../../SNIPS/KOTLIN/more_or_less.kt](More or less example)   
## WHEN
    [../../SNIPS/KOTLIN/days.kt](when example)   
    when is similar to a switch in JAVA.

## STRINGS
    [strings.md](Strings examples)

## MULTIPLE VAR AT ONCE
 String str1 = "Hello" , str2 = "World"; 

## COMPILER
    Download https://github.com/JetBrains/kotlin
    unzip archive and just set directory to path
 * Build:
    kotlinc tobig_tosmall.kt -include-runtime -d tobig_tosmall.jar    

## VAR
 var(can be modified)  val(constant) 

* Define var later:
        var name: String
        name = "Jack"
        println(name)

 <Byte> datatype can store whole number between -128 to 127. 
 <Short> can store whole number between -32768 to 32767.
 <Int> can store whole number between -2147483648 to 2147483647.
 <Long> can store whole number between -9223372036854775808L to 9223372036854775807L.
 <Float> and <Double> represent number such as 9.99F(F for Float) 3.1415
    The precision of float is only six or seven digit after the coma.
    Double have a preciosion of 15 after the coma.

The <char> datatype is used to store a single character.
    val myGrade: Char = 'B'

* Convert a numeric data type to another type,you must use one of the following functions:
    toByte() / toShort() / toInt()
    toLong() / toFloat() / toDouble()
    toChar()

## OOP (Object-Oriented-Programming)
    OOP is faster and easier to execute
    OOP provides a clear structure for the programs
    OOP help to keep the Kotlin code DRY(Don't Repeat Yourself)
        and makes the code easier to maintain, modify and debug
    OOP makes it possible to create full reusable applications with less code 
        and shorter devellopment time.

## OPERATORS
 * Arithmetic operators: 
       var sum1 = 100
       var sum2 = 250
       var x = sum1 + sum2
 
 * Increment / Decrement → ++x --x      x +=5

* Logical operators:
    &&  (AND)   Returns True if both statements are true
    !!  (OR)    Return True if one of the statements are true
    !   (NOT)   Reverse the result returns false if the result is true

## BREAK / CONTINUE
    This example skips the 4
        var i = 0
        while (i < 10) {
        if (i == 4) {
            i++
            continue
        }
         println(i)
         i++
        } 
    break statement is used to jump out of the loop.

## COMMENTS 
    //  → signle line comments
    /* */   → Double line comments
