# ---------- [ KOTLIN STRINGS ] ------------

## EXAMPLES
     [../../SNIPS/KOTLIN/Exercism/e2_Acronym.kt](Exercism Acronym exercise)      
     [../../SNIPS/KOTLIN/string.kt]()      

## USECASES
    var txt = "Hello World"
    println(txt[0])     →   FirstElement(h)
    println(txt[2])     →   Third(l)

 * Compare strings (0 is equivalent to egal)
    var txt1 = "Hello"
    var txt2 = "Hello"
    print(txt1.compareTo(txt2)) → string are similar so prints 0.

* Findind a string in a string
    var txt = "Please locate where 'locate' occurs!
    println(txt.indexOf("locate"))

* String concatenation
    var firstName = "Jack"
    var lastName = "Lemaitre"
    println(firstName + " " + lastName)

* String templates and interpolation
    var firstName = "Jack"
    var lastName = "Lemaitre"
    println("My name is $firstName $lastName")

