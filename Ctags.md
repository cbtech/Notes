# ---- [ CTAGS ] ----

  < [index.md] >

## USAGE 
* Generate TAGS file
    ctags -R -x --sort=yes --c-kinds=+p --fields=+iaS --language-force=c \
          -f <output_file> <include folder>


* More precise 
     ctags -R  --kinds-C=p --languages=c --c-kinds=+lpxz --fields=+nitazS --extras=+qr \
               --output-format=e-ctags  -f tags ~/Downloads/Github/curl-curl-7_87_0/

* LIST LANGUAGES
  ctags --list-languages
  
* LIST KINDS
  ctags --list-kinds

  l-> local variables
  c → Classes
  e → enumerators
  f → function definitions
  p → prototypes
  s → structure names
  t → typedef
  v → variable definitions
  x → Extrenal and forwardvariable declaration
  z → function parameters inside function or prototype definitions

## SDL2 TAGS
  ctags -R --languages=c++ --c++-kinds=+p --c-kinds=+flpxz \ 
        -I DECLSPEC -I SDLCALL --output-format=e-ctags -f tags /usr/include/SDL2
