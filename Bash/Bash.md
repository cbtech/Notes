# ---- [BASH SCRIPTING] ----

	<[../index.md]

* DEBUG				bash -x script

## LOOP THROUGH DIRECTORY TO COPY FILES WITHOUT DIRECTORY TREE
    find /source_directory -type f -exec mv {} /destination_directory/ \;

## DISK USAGE USING SORT
    du -sh /path/to/directory/* | sort -rh

## ARGUMENTS
```bash
 printf "%d" "$#"	→ Print the number of args
 printf "%s" "$@"	→ Print parameters

# EXAMPLE
  loop() {
    for ((i=0; i<=$#; i++)); do
      echo "parameter $i --> ${!i}"
    done
  }
```

## TERMINAL INFORMATIONS
  echo $LINES	→ Show the numbers of lines 
  echo $COLUMNS → Show the number of columns


## VAR
* DECLARE VAR
	`declare -x nLine=1`

* INCREMENT VAR
```bash
	var=$((var+1))
	((var=var+1))
	((var+=1))
	((var++))

	let "var=var+1"
	let "var+=1"
	let "var++"
```

## ECHO & PRINTF
* PRINT VAR 
```bash
	var="10"
	echo "$var"
	printf "%s" "$var"	
```
 * ECHO ON MULTIPLES LINES 
```bash
	echo "full_text":"  $full_text",\
             "status_symbol":"$status_symbol",\
	     "time":"$time",\
	     "initial_time":"$initial_time",\
	     "incr":"$incr",\
	     "running":"$running"}
									```
 * PRINT CALCUL
```bash
	now () { date --utc +%s; }
	$((60 * 25 )) or $(($(now) + 60 * 25))
```

## FOR LOOP 
```bash 
	for i in $(cmd); do
		command
	done 

Example:
	for i in {0..10}
	do
		echo "Welcome $i" 
	done 
		
```
* FOR EACH LINE IN FILE
```bash
	for i in $(cat file.txt); do
		printf "%s\n" $i
	done
```
		
## WHILE

# ---- CONDITION ----

## IF / ELSE 
		-ge 			Greater or equal
		-le 			Less or equal
```bash
				var=1
				if [ "$var" == "1" ]; then 
			 		echo "blop"
				fi
```
* Else 
```bash
		if [ ${user_input} -ge 10 ] && [ ${user_input} -le 20 ]
		then
				echo "Great! The number is 
                      what we were looking for"
		else 
				echo "WRONG"
		fi
		```

## CASE
```bash
	 case "$BLOCK_BUTTON" in
			5) decrease ;;
			4) increase ;;
			3) stop ;;
			1) start ;;
			2) pause ;;
	  esac 
```

## FUNCTIONS
func () { echo "Hello" }
func

## INCREMENT / DECREMENT 
				((var++))

## FILE
	* Check if file exist 
```bash
	FILE="file.txt"
	if test -f "$FILE"; then
		echo "$FILE exists."
	fi
```

#  ---- ARRAYS ----
`array=(Hello World)`

`array[0]=Hello`							
`array[1]=world`

`${array[*]}`					All of the items in the array
`${!array[*]}`					All of the indexes in the array
`${#array[*]}`					Number of the items in the array
`${#array[0]}`					Length of item zero


## ADD AN ELEMENT TO THE BEGINING OF ARRAY
```bash
array=("new_element" "${array[@]}") 					
array=("new_element1" "new_element2" "..." "new_elementN" "${array[@]}")
```

## ADD AN ELEMENT TO THE END OF ARRAY 
```bash
array=( "${array[@]}" "new_element" )
array+=( "new_element" )

array=( "${array[@]}" "new_element1" "new_element2" "..." "new_elementN") 
array+=( "new_element1" "new_element2" "..." "new_elementN" )
```

##  ADD AN ELEMENT TO THE SPECIFIC INDEX 
```bash
array=( "${array[@]:0:2}" "new_element" "${array[@]:2}" )
```

## REMOVE AN ELEMENT FROM ARRAY 
arr=( "${arr[@]:0:2}" "${arr[@]:3}" )

* Explanation: Remove the element #3.
		 ${arr[@]:0:2} will get two elements arr[0] and arr[1] starts from the beginning of the array.
		 ${arr[@]:3} will get all elements from index3 arr[3] to the last.

# --- [ EXAMPLES] ---
## READ FILE LINE BY LINE AND STORE INTO ARRAY
```bash
			 file="voc.txt"
			 while read -r line;do
			 l+=( "$line" )
			done < "$file"
 ```
