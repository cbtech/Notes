# -----[ SED ] -----
                                                
              <[index.md]

## OPTIONS
	-n Quiet 
	-e script expression 
	-f script file 
	-s consider file as separate rather 
	-i edit file in place
	
## SUBSTITION
 Example File: [../SNIPS/BASH/todo](Todo manager with date/blank line/color)
 sed 's/First/Second/' < oldFile > newFile
 
 * SED ON ECHO COMMAND
	echo "LEMAITRE" | sed 's/LEMAITRE/SORRY/'

 * Substition with color 
    sed -i "1s/.*/\x1B[7;37mHello/" $file
        1s      →   first line
        .*      →   replace everything
        \x1B    →   similar to \e
        [7;37m  →   Color
 * Blank a line
    sed -i "1s/.*//" $file

 * Insert Date 
    sed -i "1s|.*|\x1B[7;37mHello $(date +"%D")\x1B[0m|" $file

## USEFUL SED COMMANDS
### Change al include <config.h> in include "config.h"
    find . -type f \( -name "*.c" -o -name "*.h" \) -exec sed -i 's/#include <config.h>/#include "config.h"/g' {} +

* Paste line(15 to 25) on termbin
    sed -n '15,25p' | tee | nc termbin.com 9999

* PRINT SPECIFIC LINE(3rd of file link)
	sed -n '3p' link

* INSERT AT FIRST LINE IN FILE INT FILE YDOTOOL.c
	sed -i '1i\cland ydotoolc -o ydotool\n' ydotool.c

* Change simple occurence in file called startweston
  sed -i 's/weston/sway/' startweston

* Change all `plist` in ../../libplist/include/plist in all h files
  `find . -type f -name "*.[hc]" -exec sed -i 's#include <plist#include 
                                   <../../libplist/include/plist#' {} \;`

* Change without find
	`sed -i 's#include <curl#include <../../curl/include/curl' 
	         Source/WebCore/platform/network/ResourceError.h`

* Show all .h files
				`find . -type f -name "*.h" -print`
* Show All line containing #include in all .h files
				`sed -n '/#include/p' $(find . -type f  -name "*.h")`	
* Comment line number 88 with #define
				`sed -i '88 s*^*//*` dir/dir/file.h 

* Uncomment 
	 			`sed -i '88 s*//**` dir/dir/file.h

* COMMENT LINE CONTAINING SPECIFIC STRING 
				`sed -i '/<pattern>/s/^/#/g' file`

* UNCOMMENT IT:
				`sed -i '/<pattern>/s/^#//g' file`

## REPLACEMENT WITH SED 

* Change all <C> TO $cmd  AND ALL <T> in  ${text} in string:
		`sed -e "s/<C>/${cmd}/g;s/<T>/${text}/g;`

* Remove keywords in string:
		`sed -i 's/bad//g' ${string}`
			
