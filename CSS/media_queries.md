# ---- [ CSS - MEDIA_QUERIES ] ----


# Multiple CSS With media-queries →
    *RESOURCES*
        https://css-tricks.com/snippets/css/media-queries-for-standard-devices/
    
    *DIFFERENT CSS FILES*
        <link rel="stylesheet" media='screen and (min-width: 140px) and (max-width: 380px)' href="phone.css"/>
        <link rel="stylesheet" media='screen and (min-width: 381px) and (max-width: 700px)' href="tablet.css"/>

    The following example changes the background-color to lightgreen if the
    viewport is 480 pixels wide.If the viewport is less than 480 pixels, 
    the background-color is pink.

    @media screen and (min-width: 480px) {
    body {
        background-color: lightgreen;
    }

## 0 to 500px
    @media only screen and (max-width: 500px){}
## 501px to 600px 
    @media only screen and (max-width: 600px) and (min-width: 501px){


## IPAD PORTRAIT LANDSCAPE 
    @media all and (device-width: 768px) and (device-height: 1024px) and (orientation:portrait) {
      .ipad-portrait { color: red; } /* your css rules for ipad portrait */
    }
    @media all and (device-width: 1024px) and (device-height: 768px) and (orientation:landscape) {
      .ipad-landscape { color: blue; } /* your css rules for ipad landscape */
    }

        /* Smartphones (portrait and landscape) ----------- */
        @media only screen
        and (min-device-width : 320px)
        and (max-device-width : 480px) {
        /* Styles */
        }

        /* Smartphones (landscape) ----------- */
        @media only screen
        and (min-width : 321px) {
        /* Styles */
        }

        /* Smartphones (portrait) ----------- */
        @media only screen
        and (max-width : 320px) {
        /* Styles */
        }

        /* iPads (portrait and landscape) ----------- */
        @media only screen
        and (min-device-width : 768px)
        and (max-device-width : 1024px) {
        /* Styles */
        }

        /* iPads (landscape) ----------- */
        @media only screen
        and (min-device-width : 768px)
        and (max-device-width : 1024px)
        and (orientation : landscape) {
        /* Styles */
        }

        /* iPads (portrait) ----------- */
        @media only screen
        and (min-device-width : 768px)
        and (max-device-width : 1024px)
        and (orientation : portrait) {
        /* Styles */
        }

        /* Desktops and laptops ----------- */
        @media only screen
        and (min-width : 1224px) {
        /* Styles */
        }

        /* Large screens ----------- */
        @media only screen
        and (min-width : 1824px) {
        /* Styles */
        }

        /* iPhone 4 ----------- */
        @media
        only screen and (-webkit-min-device-pixel-ratio : 1.5),
        only screen and (min-device-pixel-ratio : 1.5) {
        /* Styles */
        }

