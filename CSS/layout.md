# ---- [ CSS - Layout ] ----

## GRID LAYOUT
    fr unit: is Fractionnal Unit

    grid-template-columns: 1fr 1fr 1fr 1fr 1fr;
    grid-template-columns: repeat(5, 1fr);
        
## POSITION
    static:
        It is always positionned according to the normal flow of the page

    relative:
        Depends of the left right top bottom properties

    fixed:
        always stays in the same page if the page is scrolled

    absolute: 
        is positionned relative to the nearest positioned ancestor
        However , if an absolute positioned element has no positioned ancestor,
        it uses the document body and moves along with page scrolling.


padding: top right bottom left
    *Example:*
        padding:10px 5px 15px;

            top padding is 10px
            right and left padding are 5px
            bottom padding is 15px
