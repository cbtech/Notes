# ---- [ ASSEPMBLY ] ----

    <[index.md]

## SECTION
    .text is for code
    .data is for constant data
    .bss(block start by symbol) used for uninitalized var

## 
  db define byte
  Word:         2-byte data item
  Doubleword:   a 4-byte(32bit) data item
  Quadword:     a 8 byte(62 bit) data item
  Paragraph     a 16 byte(128 bit) area
  Kilobyte:     1024 bytes
  Megabyte:     1,048,576

## FLAGS
    Flags like register can hold data. Flags only hold 1 bit each
    They are either true or false. Individual flags are parts of a
    larger register.

    
    Flag symbol     Description 
    ----------------------------
        CF          Carry
        PF          Parity
        ZF          Zero
        SF          Sign    
        OF          Overflow
        AF          Adjust
        IF          Interrupt Enabled

## POINTERS
    they hold its memory address 

    rip(eip,ip)         Index Pointer      
        Points to the next address to be executed on the control flow
    rsp(esp,sp)         Stack Pointer    
        Points to the top address of the stack
    rbp(ebp,bp)         Base Pointer          
        Points to the bottom of the stack

    Control flow:
      All code runs from top to bottom by default.
      The direction a program flows is called 
      the control flow. The rip register holds the
      address of the next instruction to executed.
      After each instruction, it is incremented by 1,
      making the control flow naturally flow from 
      top to bottom.

    Example
        mov rax, 1      ; rip = x
        mov rdi,1       ; rip = x+1
        mov rsi, text   ; rip = x+2
        ...

    _start:
        jmp _start      ; Loads the value "_start" 
                        ; into the rip register
                        ; Create an infinite loop
## COMPARAISSON (JUMP/CALLS)
    cmp a,b         
     after a comparaison is made, certain flags 
     are set

        a = b   ZF = 1
        a != b  ZF = 0
          -     SF = msb(a-b) ;Most Significant Bit

        Signed  Unsigned    Result
        je                      a =  b
        jne                     a != b
        jg          ja          a >  b
        jge         jae         a >= b
        jl          jb          a <  b
        jle         jbe         a <= b
        jz                      a =  0
        jnz                     a != 0
        jo                   Overflow occured
        jno                  Overflow did not occur
        js                   Jump if signed
        jns                  Jump if not signed

        cmp rax,23
        je _doThis

        Call and jumps are essentially the same
    
    Example(SUBROUTINE):
        _start:
            call _printHello
            mov rax,60
            mov rdi,0
            syscall

            _printHello
                ; blahblah
                syscall
                ret


## REGISTERS

    The default registers can be treated as pointers
    To treat a register as a pointer,surroubd the register name with
    square brackets, such as ,rax become [rax]

        mov rax,rbx → Loads the value in the rbc register into the rax
                      register

    movq %rax, %rdx             Move the content of the RAX to the RDX register
    movq 13(%rax), %rdx         Load the 8-byte value in the address
                                [RAX+13] from memory to the RDX register
    movq 0x47(%rax,%rcx,),%rdx  Loads the 8-byte value in adress
                                [RAX+RCX+0x47]from memory to the RDX
                                reigster
 * representation 
   64 bit register(RAX)
    00000000    000000000       00000000    00000000    
    ----------------------- EAX ---------------------  
                               --  AX --    -- AL --
   00000000    00000000        000000000    00000000
 64 bits:       
 
    rax(r0)      Accumulator  
    rcx(r1)      Counter
    rdx(r2)      Data
    rbx(r3)      Base
    rsp(r4)      Stack Pointer 
    rbp(r5)      Base Pointer
    rsi(r6)      Source index for str operation
    rdi(r7)      dest index for str operation
    r8 ... r15   General purpose
  
  32 bits:r0 d stands for D(ouble)Word
    eax(r0d)
    ecx(r1d)
    edx(r2d)
    ebx(r3d)
    esp(r4d)
    ebp(r5d)
    esi(r6d)
    edi(r7d)
    r8d ... r15d
 
  16 bits(lowest) w stands Word
    ax(r0w)             
    cx(r1w)
    dx(r2w)
    bx(r3w)
    sp(r4w)
    bp(r5w)
    si(r6w)
    di(r7w)

    8bits(lowest) b stand for Byte
    al(r0b)
    cl(r1b)
    dl(r2b)
    bl(r3b)
    spl(r4b)
    bpl(r5b)
    sil(r6b)
    dil(r7b)
