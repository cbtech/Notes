# ---- [ MPV ] ----

## LIST SOUND DEVICE DRIVER 
  mpv -audio-device=help

## RUN SPECIFIC SOUND DRIVER
  mpv --ao=alsa file.mp4

## PLAY MOVIE UNDER FRAMEBUFFER(DRM)
    mpv --vo=drm file.mp4

## PLAY MUSIC RANDOMLY
    mpv --suffle --audio-display=no <Folder> or <Song>

## USING CONFIGURATION FILE
 ~/.config/mpv/mpv.conf
 audio-device=alsa/default

## Play only audio
    mpv --no-video Tutorials/JustinSung

#   KODI (Raspberry pie)
 if movies is sluggish , disable buffering -> settings / services /caching


